#include <iostream>
using namespace std;


#include "data_structures/graphs/overlay.h"
#include "data_structures/graphs.h"
#include "data_structures/graphs/partition.h"
#include "tools/commandLineParser.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>

// Benutzung:
// - g Graphname
// - s Skip (in welchem Intervall sollen Knoten ausgegeben werden? (Standard: 1, alles wird ausgegeben)
// - l Anzahl der Level

int main(int argn, char **argv) {
	tools::commandLineParser clp(argn, argv);
	unsigned skip = 1;
	unsigned numLevel = 1;

	// Wenn kein Graph angegeben wurde, mit einem Fehler abbrechen.
	if (!clp.isSet("g")) {
		cout << "Kein Graph angegeben. Bitte mit `" << argv[0] << " -g <graph>` aufrufen." << endl;
		return 0;
	}

	if (clp.isSet("s")) {
		skip = atoi(clp.value<string>("s", "").c_str());
	}
	if (clp.isSet("l")) {
		numLevel = atoi(clp.value<string>("l", "").c_str());
	}

	const string s(clp.value<string>("g", ""));
	const string l(s + ".log");

	// Bastle Testgraph
	graphs::roadGraph G(s);
	graphs::partition P(s);

	// Open Logfile
	ofstream f(l.c_str());
	assert(f.is_open());

	f << "n #Knotennummer, ZellenID MatrixIndex in jewiligem Level (implizit durch Zeile)" << endl;
	for (types::NodeID u; u < G.get_num_nodes(); u++) {
		if ((u % skip) == 0) {
			f << "n " << u << endl;
			for (int i = 0; i < numLevel; i++) {
				f << P.getCell(u, i) << " " << P.getMatrixIndex(u, i) << endl;
			}
			f << "" << endl;
		}
	}

	f.close();

	return 0;
}
