/*
 * typedefs.h
 *
 *  Created on: Nov 2, 2010
 *      Author: pajor
 */

#ifndef TYPEDEFS_H_
#define TYPEDEFS_H_

#include <limits>
#include <list>

namespace types {

typedef unsigned ConnectionID;
typedef unsigned EdgeID;
typedef unsigned NodeID;
typedef unsigned TrainID;
// Können -1 werden
typedef int CellID;
typedef int LevelID;
typedef unsigned MetricID;
typedef unsigned EdgeWeight;
typedef unsigned TimePoint;
typedef unsigned short StationID;
typedef list<NodeID> nodeContainer;

// Direction (forward / backward)
enum direction {
	FORWARD_DIR = 0,
	BACKWARD_DIR = 1
};


const EdgeWeight PERIOD = 60*60*24;

// Various default values
const EdgeWeight INFTY = std::numeric_limits<EdgeWeight>::max();
const ConnectionID NULLCONNECTION = std::numeric_limits<ConnectionID>::max();

const NodeID NULLNODE = std::numeric_limits<NodeID>::max();
const StationID NULLSTATION = std::numeric_limits<StationID>::max();
const TrainID NULLTRAIN = std::numeric_limits<TrainID>::max();
const TimePoint NULLTIME = std::numeric_limits<TimePoint>::max();



#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

}

#endif /* TYPEDEFS_H_ */
