/*
 * graphs.h
 *
 *  Created on: Nov 4, 2010
 *      Author: pajor
 */

#ifndef GRAPHS_H_
#define GRAPHS_H_

// Typedefs
#include "typedefs.h"

// Road stuff
#include "graphs/properties/basicProperties.h"
#include "graphs/nodes/basicNode.h"
#include "graphs/edges/basicEdge.h"
#include "graphs/edges/indEdge.h"
#include "graphs/connections/basicConnection.h"

// Graph-Klasse
#include "graphs/dynamicGraph.h"

// Makros
#include "graphs/makros.h"


namespace graphs {

// Road graphs
typedef dynamicGraph<basicProperties, basicNode, indEdge, basicConnection> roadGraph;


}

#endif /* GRAPHS_H_ */
