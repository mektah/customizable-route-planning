/*
 * metric.h
 *
 *  Created on: Dec 16, 2011
 *      Author: ameier
 */

#ifndef METRIC_H_
#define METRIC_H_
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <assert.h>
#include "../../graphs.h"
#include "../../typedefs.h"
#include "../../../io/container_io.h"

namespace graphs {

template<typename graphType>
class metric {

public:

// Konstruktoren
metric(graphType *G) {
	this->G = G;
	edgeWeights.clear();
}
metric(string metricFileName) {
	G = NULL;
	load(metricFileName);
}
metric(size_t numEdges) {
	G = NULL;
	vector<types::EdgeWeight>(numEdges).swap(edgeWeights);
}

// Gewicht einer Kante zurückgeben
types::EdgeWeight getWeight(types::EdgeID e) {
	if (G!=NULL) {
		assert(e <= G->get_num_edges());
		return (G->edge(e)).weight();
	} else {
		assert(e < edgeWeights.size());
		return edgeWeights[e];
	}
}
// Einer Kante ein Gewicht zusweisen
void setWeight(types::EdgeID e, types::EdgeWeight w) {
	if (G!=NULL) {
		assert(e < G->get_num_edges());
		(G->edge(e)).set_weight(w);
	} else {
		assert(e < edgeWeights.size());
		edgeWeights[e] = w;
	}
}

// *** Serialisierung ***
void save(const string filename, const bool verbose = true) {
	// Don't try to save to original metric!
	assert(G==NULL);
	io::write_vector(edgeWeights, filename, verbose);
}
void load(const string filename, const bool verbose = true) {
	G = NULL;
	edgeWeights.clear();
	io::read_vector(edgeWeights, filename, verbose);
}

private:

// Wichtig: Nur genau eine der beiden Datenstrukturen wird verwendet!
graphType *G;
vector<types::EdgeWeight> edgeWeights;
};

}

#endif /* METRIC_H_ */
