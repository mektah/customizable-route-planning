/*
 * basicNode.h
 *
 *  Created on: Nov 15, 2010
 *      Author: pajor
 */

#ifndef BASICNODE_H_
#define BASICNODE_H_


#include <iostream>
#include <fstream>
#include <string>
#include <cassert>

using namespace std;

#include "../../typedefs.h"

namespace graphs {

class basicNode {
public:

	inline static string id() {
		return "basicNode 1";
	}

	inline types::EdgeID first_edge() const {
		return _first_edge;
	}
	inline types::EdgeID last_edge() const {
		return _last_edge;
	}
	inline unsigned num_edges() const {
		return last_edge() - first_edge() + 1;
	}

	inline void set_first_edge(const types::EdgeID e) {
		_first_edge = e;
	}
	inline void set_last_edge(const types::EdgeID e) {
		_last_edge = e;
	}

	// IO
	void write(ofstream &os) const {
		assert(os.is_open());
		os.write(reinterpret_cast<const char*>(this), sizeof(*this));
	}
	void read(ifstream &is) {
		assert(is.is_open());
		is.read(reinterpret_cast<char*>(this), sizeof(*this));
	}

		// Consistency stuff
	unsigned get_errors(const bool verbose = false) {
		// last_edge has to be at most 1 smaller than first_edge
		if (last_edge() + 1 < first_edge()) {
			if (verbose) cout << "ERROR: Node has last_edge " << last_edge() << " which is smaller than first_edge " << first_edge() << "." << endl;
			return 1;
		}
		return 0;
	}


	// Standard constructor
	basicNode() : _first_edge(1), _last_edge(0) {}

private:
	types::EdgeID _first_edge;
	types::EdgeID _last_edge;
};

}


#endif /* BASICNODE_H_ */
