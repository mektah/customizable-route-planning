/*
 * dynamicGraph.h
 *
 *  Created on: Nov 15, 2010
 *      Author: pajor
 */

#ifndef DYNAMICGRAPH_H_
#define DYNAMICGRAPH_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

#include "../typedefs.h"
#include "../../io/string_io.h"
#include "../pqueues/kheap.h"


namespace graphs {

template<typename PropertiesType, typename NodeType, typename EdgeType, typename ConnectionType>
class dynamicGraph {

public:

	typedef std::size_t size_t;
	typedef PropertiesType propertiesType;
	typedef NodeType nodeType;
	typedef EdgeType edgeType;
	typedef ConnectionType connectionType;
	typedef pqueues::kheap<4, types::NodeID, types::EdgeWeight> queueType;
	typedef dynamicGraph<PropertiesType, NodeType, EdgeType, ConnectionType> thisType;


public:

	// Create an empty graph
	dynamicGraph(const double es = 1.0, const double cs = 1.0) : edge_stretch(es), conn_stretch(cs) {
		init(0,0,0);
	}
	dynamicGraph(const string filename, const double es = 1.0, const double cs = 1.0) : edge_stretch(es), conn_stretch(cs) {
		init(0,0,0);
		load(filename);
	}

	// This is the graph class
	inline std::string graph_class() const {
		return _graph_class;
	}

	void set_graph_class(const std::string c) {
		_graph_class = c;
	}

	// Modify graph properties through this function
	propertiesType &props() {
		return properties;
	}


	// Retrieve a certain node by node-id
	nodeType &node(const types::NodeID u) {
		return nodes[u];
	}

	// Retrieve a certain edge by its edge-id
	edgeType &edge(const types::EdgeID e) {
		return edges[e];
	}

	// Retrieve a certain connection by its connection-id
	connectionType &connection(const types::ConnectionID c) {
		return connections[c];
	}


	// Number of nodes
	inline size_t get_num_nodes() const {
		return num_nodes;
	}

	// Number of edges
	inline size_t get_num_edges() const {
		return num_edges;
	}

	// Number of (forward) arcs
	inline size_t get_num_arcs(const types::direction dir = types::FORWARD_DIR) const {
		size_t num_arcs(0);
		for (types::NodeID u = 0; u < get_num_nodes(); ++u) {
			for (types::EdgeID e = nodes[u].first_edge(); e <= nodes[u].last_edge(); ++e) {
				if (edges[e].has_direction(dir)) ++num_arcs;
			}
		}
		return num_arcs;
	}

	// Number of connections
	inline size_t get_num_connections() const {
		return num_connections;
	}


	// Get the first incident edge to a node
	inline edgeType *get_first_edge(const types::NodeID u) {
		return &edges[nodes[u].first_edge()];
	}
	inline edgeType *get_first_edge() {
		return &edges[0];
	}
	inline edgeType *get_last_edge(const types::NodeID u) {
		return &edges[nodes[u].last_edge()];
	}
	inline edgeType *get_last_edge() {
		return &edges[get_num_edges()-1];
	}

	// Get the first incident connection for an edge
	inline connectionType *get_first_connection(const edgeType *e) {
		return &connections[e->first_connection()];
	}
	inline connectionType *get_last_connection(const edgeType *e) {
		return &connections[e->last_connection()];
	}
	inline connectionType *get_first_connection() {
		return &connections[0];
	}
	inline connectionType *get_last_connection() {
		return &connections[get_num_connections()-1];
	}


	// Checks whether an arc is contained in the graph
	inline bool contains_arc(const types::NodeID u, const types::NodeID v, const types::direction dir = types::FORWARD_DIR) {
		for (edgeType *e = get_first_edge(u); e <= get_last_edge(u); ++e) {
			if (!e->has_direction(dir)) continue;
			if (e->node() == v) return true;
		}
		return false;
	}
	// Return an arc from u to v. If the arc is not contained in the graph, returns NULL!
	inline edgeType *get_arc(const types::NodeID u, const types::NodeID v, const types::direction dir = types::FORWARD_DIR) {
		for (edgeType *e = get_first_edge(u); e <= get_last_edge(u); ++e) {
			if (!e->has_direction(dir)) continue;
			if (e->node() == v) return e;
		}
		return NULL;
	}


	// Get the ID of an edge given an edge pointer
	inline types::EdgeID get_edge_id(const edgeType *e) const {
		return e - &edges[0];
	}

	// Get the ID of a connection given a connection pointer
	inline types::ConnectionID get_connection_id(const connectionType *c) const {
		return c - &connections[0];
	}




// *** Serialization ***


	// Save graph to disk
	void save(const string filename, const bool verbose = true) {
		// Compress the graph
		compress(verbose);
		assert(get_errors(verbose) == 0);

		if (verbose) cout << "Writing graph to " << filename << "... " << flush;
		// Open file
		ofstream f(filename.c_str(), ios::binary);
		assert(f.is_open());

		// Write the graph's class
		io::write_string_binary(_graph_class, f);

		// Write file header.
		file_header h(get_num_nodes(), get_num_edges(), get_num_connections());
		h.write(f);

		// Write graph properties
		properties.write(f);

		// Write nodes
		for (types::NodeID u = 0; u < get_num_nodes(); ++u)
			nodes[u].write(f);

		// Write edges
		for (types::EdgeID e = 1; e <= get_num_edges(); ++e)
			edges[e].write(f);

		// Write connections
		for (types::ConnectionID c = 1; c <= get_num_connections(); ++c)
			connections[c].write(f);

		// Close fstream
		f.close();
		if (verbose) cout << "done." << endl;
	}

	// Load graph from disk
	void load(const string filename, const bool verbose = true) {
		if (verbose) cout << "Reading graph from " << filename << "... " << flush;
		// Open file for reading
		ifstream f(filename.c_str(), ios::binary);
		assert(f.is_open());

		// Read the graph's class
		io::read_string_binary(_graph_class, f);

		// Attempt to read file header
		file_header h;
		h.read(f);
		//if (verbose) cout << h << endl;

		// Resize nodes, edges and connections
		nodes.resize(h.num_nodes);
		num_nodes = h.num_nodes;
		first_free_node = num_nodes;
		edges.resize(h.num_edges * edge_stretch + 1 + 1); // the additional +1 is only for when the last node does not have any outgoing edges.
		edges[0].unset_valid();
		num_edges = h.num_edges;
		first_free_edge = num_edges + 1;
		connections.resize(h.num_connections * conn_stretch + 1 + 1); // same as above
		connections[0].unset_valid();
		num_connections = h.num_connections;
		first_free_connection = num_connections + 1;

		// Read the graph properties
		properties.read(f);

		// Read Nodes
		for (types::NodeID u = 0; u < get_num_nodes(); ++u)
			nodes[u].read(f);

		// Read edges
		for (types::EdgeID e = 1; e <= get_num_edges(); ++e)
			edges[e].read(f);

		// Read connections
		for (types::ConnectionID c = 1; c <= get_num_connections(); ++c)
			connections[c].read(f);

		f.close();

		if (verbose) cout << "done." << endl << get_info() << endl;


		// Perform a consistency check (!)
		assert(get_errors(verbose) == 0);
	}

	string get_info() const {
		stringstream ss;
		ss << "Graph [" << id() << "] info: " << get_num_nodes() << " nodes [" << nodeType::id() << "], "
				<< get_num_edges() << " edges [" << edgeType::id() << "], "
				<< get_num_arcs() << " arcs, "
				<< get_num_connections() << " connections [" << connectionType::id() << "]";
		return ss.str();
	}



// *** Dynamic functions ***

	// Clear the graph
	void clear() {
		init(0,0,0);
	}

	// Add a node to the graph and return its new Node-ID
	types::NodeID add_node() {
		assert(first_free_edge > 0);

		// This resizes the nodes-array on requirement.
		if (first_free_node <= nodes.size()) nodes.resize(first_free_node + 1);
		nodes[first_free_node].set_first_edge(first_free_edge);
		nodes[first_free_node].set_last_edge(first_free_edge-1);
		++num_nodes;
		return first_free_node++;
	}

	// Add a lot of nodes to the graph.
	void add_nodes(const size_t n) {
		for (size_t i = 0; i < n; ++i) {
			add_node();
		}
	}

	// Deletes a node in the sense, that it isolates the node (we do not remap node ids)
	void delete_node(const types::NodeID u) {
		nodeType &n = nodes[u];

		// Iterate over all nodes, and delete
		for (types::EdgeID e = n.last_edge(); e >= n.first_edge(); --e) {
			delete_edge(u, &edges[e]);
		}

		// At this point this node must not have any incident edges
		assert(n.num_edges() == 0);
	}

	// Add an edge to the graph from u to v, return a pointer
	// to the newly created edge.
	// Warning, may create multi-edges
	edgeType *add_edge(const types::NodeID u, const types::NodeID v) {
		nodeType &n = nodes[u];

		// Check if there is space right of last_pointer.
		types::EdgeID new_edge_id = n.last_edge() + 1;
		if (new_edge_id < edges.size() && !edges[new_edge_id].valid()) {
			assert(new_edge_id > 0);
			edges[new_edge_id] = edgeType();
			edges[new_edge_id].set_valid();
			edges[new_edge_id].set_node(v);
			n.set_last_edge(new_edge_id);
			first_free_edge = max(first_free_edge, new_edge_id + 1);
			++num_edges;
			return &edges[new_edge_id];
		}

		// Check if there is space left of first_pointer.
		new_edge_id = n.first_edge() - 1;
		if (new_edge_id > 0 && !edges[new_edge_id].valid()) {
			assert(new_edge_id > 0);
			edges[new_edge_id] = edgeType();
			edges[new_edge_id].set_valid();
			edges[new_edge_id].set_node(v);
			n.set_first_edge(new_edge_id);
			++num_edges;
			return &edges[new_edge_id];
		}

		// We have no more room for the new edge, thus, copy all edges of u
		// to the end of the edges array
		if (edges.size() - first_free_edge <= n.num_edges()) {
			// resize the edges array here (how?)
			// currently, greedy.
			edges.resize(first_free_edge + n.num_edges() + 1);
		}

		// Copy edges to first_free_edge ---> first_free_edge + num_inc_edges + 1;
		// But only if our nodes is not at the "end" of the occupied part of the edges
		// array. In that case, it was sufficient to just extend the array.
		if (n.last_edge() != first_free_edge - 1) {
			new_edge_id = first_free_edge + n.num_edges();
			for (unsigned i = 0; i < n.num_edges(); ++i) {
				assert(!edges[first_free_edge + i].valid());
				assert(edges[n.first_edge() + i].valid());
				edges[first_free_edge + i] = edges[n.first_edge() + i];
				edges[n.first_edge() + i].unset_valid();
			}
			nodes[u].set_first_edge(first_free_edge);
		} else {
			new_edge_id = n.last_edge() + 1;
		}
		nodes[u].set_last_edge(new_edge_id);
		assert(new_edge_id > 0);
		edges[new_edge_id] = edgeType();
		edges[new_edge_id].set_valid();
		edges[new_edge_id].set_node(v);
		++num_edges;
		first_free_edge = new_edge_id + 1;
		return &edges[new_edge_id];
	}

	// Delete e certain edge by passing a pointer to the specific
	// edge that should be deleted.
	void delete_edge(const types::NodeID u, edgeType *e) {
		assert(e->valid());
		nodeType &n = nodes[u];
		types::EdgeID e_id = get_edge_id(e);

		// If this is an edge with adjacent connections, delete all
		// connections incident to this edge.
		if (!e->constant()) {
			for (types::ConnectionID c = e->last_connection(); !e->constant() && c >= e->first_connection(); --c) {
				delete_connection(e, &connections[c]);
			}
		}

		// At this point the edge must be constant.
		assert(e->constant());

		// If the edge is not the last edge of node, u copy over the
		// last edge into its slot and make the last edge invalid
		if (e_id != n.last_edge()) {
			edges[e_id] = edges[n.last_edge()];
			edges[n.last_edge()].unset_valid();
		} else {
			// otherwise just make the edge itself invalid
			edges[e_id].unset_valid();
		}

		// Decrease the last edge by 1
		n.set_last_edge(n.last_edge()-1);
		--num_edges;
	}

	// Adds a connection given a specific edge-pointer.
	// Returns the newly created connection
	connectionType* add_connection(edgeType *e) {

		// If the edge is constant, i.e., no connections assigned to it so far,
		// begin assigning connections at the end of the connections thing.
		if (e->constant()) {
			e->unset_constant(); // remove constness
			// If there is no room for connections, resize
			if (connections.size() <= first_free_connection)
				connections.resize(first_free_connection + 1);
			types::ConnectionID new_connection_id = first_free_connection;

			// Add the connection and update first/last conection of the edge
			connections[new_connection_id] = connectionType();
			assert(!connections[new_connection_id].valid());
			connections[new_connection_id].set_valid();
			e->set_first_connection(new_connection_id);
			e->set_last_connection(new_connection_id);
			++num_connections;
			++first_free_connection;
			return &connections[new_connection_id];
		}

		// Check if there is space right of last_pointer.
		types::ConnectionID new_connection_id = e->last_connection() + 1;
		if (new_connection_id < connections.size() && !connections[new_connection_id].valid()) {
			connections[new_connection_id] = connectionType();
			connections[new_connection_id].set_valid();
			e->set_last_connection(new_connection_id);
			++num_connections;
			first_free_connection = max(first_free_connection, new_connection_id + 1);
			return &connections[new_connection_id];
		}

		// Check if there is space left of first_pointer.
		new_connection_id = e->first_connection() - 1;
		if (new_connection_id > 0 && !connections[new_connection_id].valid()) {
			connections[new_connection_id] = connectionType();
			connections[new_connection_id].set_valid();
			e->set_first_connection(new_connection_id);
			++num_connections;
			return &connections[new_connection_id];
		}

		// We have no more room for the new connection, thus, copy all connections of e
		// to the end of the connections array
		if (connections.size() - first_free_connection <= e->num_connections()) {
			connections.resize(first_free_connection + e->num_connections() + 1);
		}

		// Copy connections to first_free_connection ---> first_free_connection + num_connections + 1;
		// But only if our edge is not at the border of the connections array.
		if (e->last_connection() + 1 != first_free_connection) {
			new_connection_id = first_free_connection + e->num_connections();
			for (unsigned i = 0; i < e->num_connections(); ++i) {
				assert(!connections[first_free_connection + i].valid());
				assert(connections[e->first_connection() + i].valid());
				connections[first_free_connection + i] = connections[e->first_connection() + i];
				connections[e->first_connection() + i].unset_valid();
			}
			e->set_first_connection(first_free_connection);
		} else {
			new_connection_id = e->last_connection() + 1;
		}
		e->set_last_connection(new_connection_id);
		connections[new_connection_id] = connectionType();
		connections[new_connection_id].set_valid();
		++num_connections;
		first_free_connection = new_connection_id + 1;
		return &connections[new_connection_id];

	}

	// Adds a connection given a specific edge-id
	connectionType* add_connection(types::EdgeID e) {
		return add_connection(&edges[e]);
	}

	// Delete a connection given a certain connection pointer
	// Returns: True if this was the last connection of the edge, false otherwise
	// WARNING: This might destroy the FIFO-Property!
	bool delete_connection(edgeType *e, connectionType *c) {
		assert(c->valid());
		types::ConnectionID c_id = get_connection_id(c);

		// If the connection is not the last connection of edge, e copy over the
		// last connection into its slot and make the last connection invalid
		if (c_id != e->last_connection()) {
			connections[c_id] = connections[e->last_connection()];
			connections[e->last_connection()].unset_valid();
		} else {
			// otherwise just make the connection itself invalid
			connections[c_id].unset_valid();
		}

		// Decrease the last connection by 1
		e->set_last_connection(e->last_connection()-1);

		--num_connections;

		// If we have no more connections now, make the edge constant with weight INFTY.
		if (e->num_connections() == 0) {
			e->set_constant();
			e->set_weight(types::INFTY);
			return true;
		}

		return false;
	}

	// External update on the number of connections
	inline void update_num_connections(int diff) {
		assert(num_connections + diff >= 0);
		num_connections += diff;
	}



// *** Consistency stuff ***


	// Perform a full consistency check of the graph
	unsigned get_errors(const bool verbose=false) {
		unsigned num_errors(0);
		if (verbose) cout << "Performing a consistency check...n..." << flush;
		// see if all nodes point to a valid first and last edge.
		unsigned num_inc_edges(0);
		for (types::NodeID u = 0; u < get_num_nodes(); ++u) {
			if (nodes[u].first_edge() >= edges.size()) {
				if (verbose) cout << "ERROR: Node " << u << " points to first edge no. " << nodes[u].first_edge() << " which is too large." << endl;
				++num_errors;
			}
			if (nodes[u].last_edge() >= edges.size()) {
				if (verbose) cout << "ERROR: Node " << u << " points to last edge no. " << nodes[u].last_edge() << " which is too large." << endl;
				++num_errors;
			}
			// Iterate over all edges in the range of this node, and check if they are valid.
			for (types::EdgeID e = nodes[u].first_edge(); e <= nodes[u].last_edge(); ++e) {
				if (!edges[e].valid()) {
					if (verbose) cout << "ERROR: Node " << u << " has an invalid incident edge " << e << " in range " << nodes[u].first_edge() << "-" << nodes[u].last_edge() << "." << endl;
					++num_errors;
				} else {
					++num_inc_edges;
				}
			}
			// Call consistency check on node
			num_errors += nodes[u].get_errors(verbose);
		}
		if (verbose) cout << "e..." << flush;
		// The number of incident edges has to equal the number of edges in the graph
		if (num_edges != num_inc_edges) {
			if (verbose) cout << "ERROR: Number of edges " << num_edges << " does not equal the number of found incident edges " << num_inc_edges << "." << endl;
			++num_errors;
		}
		// First edge must be invalid.
		if (edges[0].valid()) {
			if (verbose) cout << "ERROR: Edge 0 is valid." << endl;
			++num_errors;
		}
		unsigned num_valid_edges(0);
		unsigned num_inc_connections(0);
		for (types::EdgeID e = 0; e < edges.size(); ++e) {
			if (!edges[e].valid()) continue; // skip invalid edges.

			++num_valid_edges;

			// check whether head node exists.
			if (edges[e].node() >= nodes.size()) {
				if(verbose) cout << "ERROR: Edge " << e << " points to node no. " << edges[e].node() << " which is too large." << endl;
				++num_errors;
			}
			// If the edge is non-constant
			if (!edges[e].constant()) {
				// check whether first connection exists
				if (edges[e].first_connection() >= connections.size()) {
					if (verbose) cout << "ERROR: Edge " << e << " points to a (first) connection no. " << edges[e].first_connection() << " which is too large." << endl;
					++num_errors;
				}
				// Check whether last connection exists
				if (edges[e].last_connection() >= connections.size()) {
					if (verbose) cout << "ERROR: Edge " << e << " points to a (last) connection no. " << edges[e].last_connection() << " which is too large." << endl;
					++num_errors;
				}
				// Check if all incident connections are valid
				for (types::ConnectionID c = edges[e].first_connection(); c <= edges[e].last_connection(); ++c) {
					if (!connections[c].valid()) {
						if (verbose) cout << "ERROR: Edge " << e << " has an invalid incident connection " << c << " in range " << edges[e].first_connection() << "-" << edges[e].last_connection() << "." << endl;
						++num_errors;
					} else {
						++num_inc_connections;
					}
				}
			}
			// Call consistency check on edge
			num_errors += edges[e].get_errors(verbose);
		}
		// The number of valid edges has to equal the number of edges in the graph
		if (num_edges != num_valid_edges) {
			if (verbose) cout << "ERROR: Number of edges " << num_edges << " does not equal the number of valid edges " << num_valid_edges << "." << endl;
			++num_errors;
		}
		// The number of incident connections has to equal the number of connections in the graph
		if (num_connections != num_inc_connections) {
			if (verbose) cout << "ERROR: Number of connections " << num_connections << " does not equal the number of found incident connections " << num_inc_connections << "." << endl;
			++num_errors;
		}
		if (verbose) cout << "c..." << flush;
		// First connection has to be invalid
		if (connections[0].valid()) {
			if (verbose) cout << "ERROR: Connection 0 is valid." << endl;
			++num_errors;
		}
		unsigned num_valid_connections(0);
		for (types::ConnectionID c = 0; c < connections.size(); ++c ) {
			if (!connections[c].valid()) continue; // skip invalid connections
			++num_valid_connections;
			num_errors += connections[c].get_errors(verbose);
		}
		// The number of valid connections has to equal the number of connections in the graph
		if (num_connections != num_valid_connections) {
			if (verbose) cout << "ERROR: Number of connections " << num_connections << " does not equal the number of valid connections " << num_valid_connections << "." << endl;
			++num_errors;
		}

		// Check for self-loops
		unsigned num_self_loops(0);
		for (types::NodeID u = 0; u < nodes.size(); ++u) {
			for (edgeType *e = get_first_edge(u); e <= get_last_edge(u); ++e) {
				if (e->node() == u) ++num_self_loops;
			}
		}

		if (num_self_loops > 0) {
			if (verbose) cout << "WARNING: " << num_self_loops << " self loops found." << endl;
		}
		if (num_errors > 0) {
			if (verbose) cout << num_errors << " errors found." << endl;
		} else {
			if (verbose) cout << "OK :-)" << endl;
		}

		return num_errors;
	}


	// This compresses the graph such that there are no "holes" inside the edge
	// array and also the connections array.
	void compress(const bool verbose = false) {
		if (verbose) cout << "Compressing graph... " << flush;

		// Create temporary edge vector
		vector<edgeType> new_edges(get_num_edges() * edge_stretch + 1 + 1); // In case the last node has no edges, its first pointer is "too big"
		                                                                    // we can omit one +1, but then the consistency check has to include this exception
		types::EdgeID new_edge_id(1);

		// Iterate over all nodes and rearrange edges.
		for (types::NodeID u = 0; u < nodes.size(); ++u) {
			types::EdgeID first_edge = new_edge_id;
			types::EdgeID last_edge = first_edge - 1;
			for (types::EdgeID e = nodes[u].first_edge(); e <= nodes[u].last_edge(); ++e) {
				assert(edges[e].valid());
				new_edges[new_edge_id] = edges[e];
				last_edge = new_edge_id++;
			}
			nodes[u].set_first_edge(first_edge);
			nodes[u].set_last_edge(last_edge);
		}
		assert(new_edge_id - 1 == get_num_edges());
		new_edges.swap(edges);
		vector<edgeType>().swap(new_edges);
		first_free_edge = new_edge_id;

		// Now do the same for connections!
		vector<connectionType> new_connections(get_num_connections() * conn_stretch + 1 + 1); // same as for the edges
		types::ConnectionID new_connection_id(1);

		for (types::EdgeID e = 1; e <= get_num_edges(); ++e) {
			assert(edges[e].valid());
			if (edges[e].constant()) continue;

			types::ConnectionID first_connection = new_connection_id;
			types::ConnectionID last_connection = first_connection - 1;
			for (types::ConnectionID c = edges[e].first_connection(); c <= edges[e].last_connection(); ++c) {
				assert(connections[c].valid());
				new_connections[new_connection_id] = connections[c];
				last_connection = new_connection_id++;
			}
			edges[e].set_first_connection(first_connection);
			edges[e].set_last_connection(last_connection);
		}
		assert(new_connection_id - 1 == get_num_connections());
		new_connections.swap(connections);
		vector<connectionType>().swap(new_connections);
		first_free_connection = new_connection_id;

		if (verbose) cout << "done." << endl;
	}




protected:

	// Initialize the graph
	void init(const size_t n, const size_t m , const size_t c) {
		vector<nodeType>().swap(nodes);
		vector<edgeType>().swap(edges);
		vector<connectionType>().swap(connections);

		// Reserve some memory (if requested)
		nodes.reserve(n);
		edges.reserve(m * edge_stretch +1);
		connections.reserve(c * conn_stretch +1);

		// Add a dummy edge at slot 0.
		edges.resize(1);
		edges[0].unset_valid();

		// Add a dummy connection at slot 0
		connections.resize(1);
		connections[0].unset_valid();

		num_nodes = 0;
		num_edges = 0;
		num_connections = 0;

		first_free_node = 0;
		first_free_edge = 1;
		first_free_connection = 1;
	}



private:

	// The file header
	struct file_header {
		string graph_id;
		string properties_id;
		string node_id;
		string edge_id;
		string connection_id;
		unsigned int num_nodes;
		unsigned int num_edges;
		unsigned int num_connections;
		file_header() : graph_id(id()), properties_id(propertiesType::id()), node_id(nodeType::id()), edge_id(edgeType::id()), connection_id(connectionType::id()) {}
		file_header(const size_t n, const size_t m, const size_t c) : graph_id(id()), properties_id(propertiesType::id()), node_id(nodeType::id()), edge_id(edgeType::id()), connection_id(connectionType::id()),
				num_nodes(n), num_edges(m), num_connections(c) {}

		// Dump header.
		friend ostream &operator<<(ostream &os, const file_header h) {
			return os << "Graph ID: " << h.graph_id << endl
					<< "Properties ID: " << h.properties_id << endl
					<< "Node ID: " << h.node_id << endl
					<< "Edge ID: " << h.edge_id << endl
					<< "Connection ID: " << h.connection_id << endl
					<< "Number of Nodes: " << h.num_nodes << endl
					<< "Number of Edges: " << h.num_edges << endl
					<< "Number of Connections: " << h.num_connections;
		}

		// Write header to ofstream
		void write(ofstream &os) const {
			assert(os.is_open());
			// Write IDs
			io::write_string_binary(graph_id, os);
			io::write_string_binary(properties_id, os);
			io::write_string_binary(node_id, os);
			io::write_string_binary(edge_id, os);
			io::write_string_binary(connection_id, os);
			// Write number of nodes etc.
			os.write(reinterpret_cast<const char*>(&num_nodes), sizeof(num_nodes));
			os.write(reinterpret_cast<const char*>(&num_edges), sizeof(num_edges));
			os.write(reinterpret_cast<const char*>(&num_connections), sizeof(num_connections));
		}

		// Read header from ifstream
		void read(ifstream &is) {
			assert(is.is_open());
			// Read IDs
			io::read_string_binary(graph_id, is);
			assert(graph_id == id());
			io::read_string_binary(properties_id, is);
			assert(properties_id == propertiesType::id());
			io::read_string_binary(node_id, is);
			assert(node_id == nodeType::id());
			io::read_string_binary(edge_id, is);
			assert(edge_id == edgeType::id());
			io::read_string_binary(connection_id, is);
			assert(connection_id == connectionType::id());
			// Read number of nodes, etc
			is.read(reinterpret_cast<char*>(&num_nodes), sizeof(num_nodes));
			is.read(reinterpret_cast<char*>(&num_edges), sizeof(num_edges));
			is.read(reinterpret_cast<char*>(&num_connections), sizeof(num_connections));
		}
	};

	inline static string id() {
		return "graph 1";
	}


	// Data structures for the graph
	string _graph_class;
	propertiesType properties;
	vector<nodeType> nodes;
	vector<edgeType> edges;
	vector<connectionType> connections;

	// Sizes
	size_t num_nodes, num_edges, num_connections;

	// Construction related stuff
	types::NodeID first_free_node;
	types::EdgeID first_free_edge;
	types::ConnectionID first_free_connection;
	double edge_stretch, conn_stretch;

};

}

#endif /* DYNAMICGRAPH_H_ */
