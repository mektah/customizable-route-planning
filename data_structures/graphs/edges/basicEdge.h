/*
 * edge.h
 *
 *  Created on: Nov 2, 2010
 *      Author: pajor
 */

#ifndef EDGE_H_
#define EDGE_H_

#include <iostream>
#include <fstream>
#include <string>
#include <cassert>

using namespace std;


#include "../../typedefs.h"

namespace graphs {

class basicEdge {
public:

	inline static string id() {
		return "basicEdge";
	}

	inline types::NodeID node() const {
		return _node;
	}
	inline bool forward() const {
		return _forward;
	}
	inline bool backward() const {
		return _backward;
	}
	inline bool has_direction(const types::direction dir) const {
		if (dir == types::FORWARD_DIR)
			return forward();
		else
			return backward();
	}
	inline types::ConnectionID first_connection() const {
		assert(!constant());
		return _first_connection;
	}
	inline types::ConnectionID last_connection() const {
		assert(!constant());
		return _last_connection;
	}
	inline unsigned num_connections() const {
		assert(!constant());
		return _last_connection - _first_connection + 1;
	}
	inline bool constant() const {
		return _constant;
	}
	inline bool valid() const {
		return _valid;
	}
	inline types::EdgeWeight weight(const types::TimePoint dep = 0) const {
		assert(constant());
		return _weight;
	}


	inline void set_node(const types::NodeID n) {
		_node = n;
	}
	inline void set_forward() {
		_forward = true;
	}
	inline void unset_forward() {
		_forward = false;
	}
	inline void set_backward() {
		_backward = true;
	}
	inline void unset_backward() {
		_backward = false;
	}
	inline void set_first_connection(const types::ConnectionID c) {
		assert(!constant());
		_first_connection = c;
	}
	inline void set_last_connection(const types::ConnectionID c) {
		assert(!constant());
		_last_connection = c;
	}
	inline void set_constant() {
		_constant = true;
	}
	inline void unset_constant() {
		_constant = false;
		set_first_connection(types::NULLCONNECTION);
		set_last_connection(types::NULLCONNECTION);
	}
	inline void set_valid() {
		_valid = true;
	}
	inline void unset_valid() {
		_valid = false;
	}
	inline void set_weight(const types::EdgeWeight w) {
		assert(constant());
		_weight = w;
	}


	// IO
	void write(ofstream &os) const {
		assert(os.is_open());
		os.write(reinterpret_cast<const char*>(this), sizeof(*this));
	}
	void read(ifstream &is) {
		assert(is.is_open());
		is.read(reinterpret_cast<char*>(this), sizeof(*this));
	}

	// Consistency stuff
	unsigned get_errors(const bool verbose = false) {
		if (valid() && !forward() && !backward()) {
			if (verbose) cout << "ERROR: Edge has neither forward nor backward flag set." << endl;
			return 1;
		}
		return 0;
	}


	// Default constructor
	basicEdge() : _node(0), _forward(0), _backward(0), _constant(1), _valid(0), _first_connection(types::NULLCONNECTION), _last_connection(types::NULLCONNECTION) {  }

private:
	types::NodeID _node : 28;
	bool _forward : 1;
	bool _backward : 1;
	bool _constant : 1;
	bool _valid : 1;
	union {
		types::ConnectionID _first_connection;
		types::EdgeWeight _weight;
	};
	types::ConnectionID _last_connection;
};


}


#endif /* EDGE_H_ */
