/*
 * indEdge.h
 *
 *  Created on: Nov 5, 2010
 *      Author: pajor
 */

#ifndef INDEDGE_H_
#define INDEDGE_H_

#include <iostream>
#include <fstream>
#include <string>
#include <cassert>

using namespace std;


#include "../../typedefs.h"



namespace graphs {


class indEdge {

public:

	inline static string id() {
		return "indEdge 1";
	}

	inline types::NodeID node() const {
		return _node >> 3;
	}
	inline bool forward() const {
		return _node & 1;
	}
	inline bool backward() const {
		return _node & 2;
	}
	inline bool has_direction(const types::direction dir) const {
		if (dir == types::FORWARD_DIR)
			return forward();
		else
			return backward();
	}
	inline types::ConnectionID first_connection() const {
		return types::NULLCONNECTION;
	}
	inline types::ConnectionID last_connection() const {
		return types::NULLCONNECTION;
	}
	inline unsigned num_connections() const {
		return 0;
	}
	inline bool constant() const {
		return true;
	}
	inline bool valid() const {
		return _node & 4;
	}
	inline types::EdgeWeight weight(const types::TimePoint dep = 0) const {
		return _weight;
	}


	inline void set_node(const types::NodeID n) {
		_node = (n << 3) | (_node & 0x00000007);
	}
	inline void set_forward() {
		_node |= 1;
	}
	inline void unset_forward() {
		_node &= ~1;
	}
	inline void set_backward() {
		_node |= 2;
	}
	inline void unset_backward() {
		_node &= ~2;
	}
	inline void set_first_connection(const types::ConnectionID c) {
	}
	inline void set_last_connection(const types::ConnectionID c) {
	}
	inline void set_constant() {
	}
	inline void unset_constant() {
	}
	inline void set_valid() {
		_node |= 4;
	}
	inline void unset_valid() {
		_node &= ~4;
	}
	inline void set_weight(const types::EdgeWeight w) {
		_weight = w;
	}


	// IO
	void write(ofstream &os) const {
		assert(os.is_open());
		os.write(reinterpret_cast<const char*>(this), sizeof(*this));
	}
	void read(ifstream &is) {
		assert(is.is_open());
		is.read(reinterpret_cast<char*>(this), sizeof(*this));
	}

	// Consistency stuff
	unsigned get_errors(const bool verbose = false) {
		if (!forward() && !backward()) {
			if (verbose) cout << "ERROR: Edge has neither forward nor backward flag set." << endl;
			return 1;
		}
		return 0;
	}


	// Default constructor
	indEdge() : _node(0), _weight(0) {}

protected:
	types::NodeID _node;
	types::EdgeWeight _weight;
};


}

#endif /* INDEDGE_H_ */
