/*
 * overlay.h
 *
 *  Created on: Dec 16, 2011
 *      Author: ameier
 */

#ifndef OVERLAY_H_
#define OVERLAY_H_
using namespace std;

#include "../typedefs.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <assert.h>
#include <string>
#include "../../io/container_io.h"
#include "partition.h"

namespace graphs {

class overlay {

typedef types::NodeID nodeID;
typedef types::CellID cellID;
typedef types::LevelID levelID;
typedef types::EdgeWeight edgeWeight;
typedef types::direction Direction;

public:

overlay(graphs::partition *p) {
	assert(p != NULL);

	cells.clear();
	//Anzahl Level
	unsigned l = p->getNumLevel();
	cellOffsets.resize(l+1);
	levelDeleted.resize(l);
	for (unsigned i = 0; i < l; i++) {
		cellOffsets[i] = (i == 0 ? 0 : cellOffsets[i-1] + p->getNumCells(i-1));
		levelDeleted[i] = false;
	}

	// Just formal ...
	cellOffsets[l] = cellOffsets[l-1] + p->getNumCells(l-1);
	cells.resize(cellOffsets[l], NULL);

	for (unsigned i = 0; i < l; i++) {
		for (unsigned c = cellOffsets[i]; c < cellOffsets[i+1]; c++) {
			cells[c] = new cell(p->getCellBorderSize(i, c - cellOffsets[i]));
			assert(cells[c] != NULL);
		}
	}

	levelDeleted.resize(l);
	this->p = p;
}

~overlay() {
	for (unsigned i = 0; i < cells.size(); i++) {
		if (cells[i] != NULL) {
			delete cells[i];
			cells[i] = NULL;
		}
	}
}

// *** Iteratorstrukturen ***
// Iterator für Shortcut-Edges eines Levels ausgehen von einem Knoten
// Vorsicht: Keine Berücksichtigung der Cut-Edges
void selectNode(types::NodeID u, types::LevelID l, Direction dir) {

	assert ((l == -1) || (p->getMaxBorderLevel(u) >= l));
	assert (!isLevelDeleted(l));

	searchLevel = l;
	if (l != -1) {
		searchSourceNode = p->getMatrixIndex(u,l);
		searchTargetNode = 0;
		searchCell = p->getCellID(u,l);
		searchDir = dir;
		assert(getCell(searchLevel, searchCell) != NULL);
	}

}

//should always return false if level is -1.
//if selected level is phantom level, should return false or throw error
bool hasShortcutEdgesLeft() {
	if (searchLevel == -1) {
		return false;
	} else if (searchSourceNode == (getCell(searchLevel, searchCell)->size() - 1)) {
		return (searchTargetNode < (getCell(searchLevel, searchCell)->size() - 1));
	} else {
		return (searchTargetNode < (getCell(searchLevel, searchCell)->size()));
	}
}

void getNextShortcutEdge(types::NodeID &v, types::EdgeWeight &w) {
	assert(hasShortcutEdgesLeft());

	v = p->matrixIndexToNodeID(searchLevel, searchCell, searchTargetNode);
	w = getCell(searchLevel, searchCell)->getDistance(searchSourceNode, searchTargetNode, searchDir);
	searchTargetNode++;
	if (searchTargetNode == searchSourceNode) {
		searchTargetNode++;
	}
}

edgeWeight getDistance(nodeID v, nodeID w) {
	levelID vl = p->getMaxBorderLevel(v);
	levelID wl = p->getMaxBorderLevel(w);
	levelID lcl = p->getLeastCommonLevel(v,w);
	//cout << "Get Distance from " << v << "(" << vl << ") to " << w << "(" << wl << ")" << endl;
	assert(lcl >= 0);
	assert(vl >= lcl);
	assert(wl >= lcl);
	assert(p->getCellID(v, lcl) == p->getCellID(w,lcl));
	//cout << "lcl:" << lcl << endl;
	return (getCell(lcl,p->getCellID(v,lcl)))->getDistance(p->getMatrixIndex(v, lcl), p->getMatrixIndex(w,lcl));
}

// Level löschen (Phantomlevel)
void deleteLevel(levelID l) {
	assert(l < cells.size());
	for (unsigned i = cellOffsets[l]; i < cellOffsets[l+1]; i++) {
		if (cells[i] != NULL) {
			delete cells[i];
			cells[i] = NULL;
		}
	}
	levelDeleted[l] = true;
}

// Ist Level gelöscht?
bool isLevelDeleted(levelID l) {
	assert((l == -1) || (l < cellOffsets.size()-1));

	if (l == -1) {
		return false;
	} else {
		return levelDeleted[l];
	}
}

// Node Matrices
struct cell {
	vector<edgeWeight> arrayContent;
	unsigned internalSize;

	cell(size_t s = 0) {
		internalSize = 0;
		arrayContent.clear();
		if (s > 0) resize(s);
	}

	inline void resize(size_t s) {
		internalSize = s;
		arrayContent.resize(s*s);
		for (unsigned i = 0; i < s*s; i++) {
			arrayContent[i] = types::INFTY;
		}
	}

	// Schmeisse Matrix-Indizes rein, bekomme Distanz der Knoten
	inline void setDistance(unsigned source, unsigned target, edgeWeight w) {
		assert(source < size());
		assert(target < size());
		arrayContent[source*internalSize + target] = w;

	}
	inline edgeWeight getDistance(unsigned source, unsigned target, Direction dir = FORWARDS) {
		assert(source < size());
		assert(target < size());

		if (dir == FORWARDS) {
			return arrayContent[source*internalSize + target];
		} else {
			return arrayContent[target*internalSize + source];
		}
	}
	// Erhalte die Anzahl der Randknoten dieser Zelle
	// Bem.: Völlig identisch zu partition.getCellBorderSize(...)
	inline unsigned size() {
		return internalSize;
	}

	//TODO: Lokale Metrik-Änderungen implementieren!
};

// Zeiger auf Zelle zurückgeben
cell *getCell(levelID l, cellID c) {
	assert(l < cellOffsets.size() - 1);
	assert(c < cellOffsets[l+1] - cellOffsets[l]);
	assert (cells[cellOffsets[l] + c] != NULL);

	return cells[cellOffsets[l] + c];
}

private:

// cells[levelID][cellID]
vector<cell *> cells;
vector<unsigned> cellOffsets;


vector<bool> levelDeleted;
// Referenz auf Partition, mit der gearbeitet wird
graphs::partition *p;
// Variablen für Iterator
Direction searchDir;
nodeID searchSourceNode;
nodeID searchTargetNode;
levelID searchLevel;
cellID searchCell;

static const Direction FORWARDS = types::FORWARD_DIR;
static const Direction BACKWARDS = types::BACKWARD_DIR;

};

}
#endif /* OVERLAY_H_ */
