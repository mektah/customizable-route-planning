/*
 * makros.h
 *
 *  Created on: Nov 15, 2010
 *      Author: pajor
 */

#ifndef MAKROS_H_
#define MAKROS_H_


namespace graphs {


// Makros for easy access to nodes, edges, etc

// Iterate over all nodes
#define FORALL_NODES(G,u) for (types::NodeID u = 0; u < G.get_num_nodes(); ++u)
// Iterate over all edges of the graph incident to some node u
#define FORALL_INCIDENT_EDGES(G,u,e) for (e = G.get_first_edge(u); e <= G.get_last_edge(u); ++e)
// Iterate over all edges, thereby retrieving its source node
#define FORALL_EDGES(G,u,e) for (types::NodeID u = 0; u < G.get_num_nodes(); ++u) for (e = G.get_first_edge(u); e <= G.get_last_edge(u); ++e)
// Iterate over all edges backwards, thereby retrieveing its source node
#define FORALL_EDGES_REVERSE(G,u,e) for (types::NodeID u = G.get_num_nodes()-1; (u+1) > 0; --u) for (e = G.get_last_edge(u); e >= G.get_first_edge(u); --e)
// Iterate over all connections belonging to an edge
#define FORALL_INCIDENT_CONNECTIONS(G,e,c) for (c = G.get_first_connection(e); c <= G.get_last_connection(e); ++c)
// Iterate over all connections
#define FORALL_CONNECTIONS(G,e,c) for (types::NodeID u = 0; u < G.get_num_nodes(); ++u) for (e = G.get_first_edge(u); e <= G.get_last_edge(u); ++e) if (!e->constant()) for (c = G.get_first_connection(e); c <= G.get_last_connection(e); ++c)


}


#endif /* MAKROS_H_ */
