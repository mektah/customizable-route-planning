/*
 * partition.h
 *
 *  Created on: Jan 5, 2012
 *      Author: ameier
 */

#ifndef PARTITION_H_
#define PARTITION_H_

using namespace std;

#include "../typedefs.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <assert.h>
#include <string>
#include "../../io/container_io.h"

namespace graphs {

// Hilfsklasse zur Speicherung der topologischen Daten einer Graphpartitionierung
class partition {

typedef types::NodeID nodeID;
typedef types::CellID cellID;
typedef types::LevelID levelID;

public:


// Konstruktor
partition(unsigned numNodes, std::size_t numLevel, vector<size_t> &numCells) {
	LEVELSIZES[0] = 20;
	LEVELSIZES[1] = 15;
	LEVELSIZES[2] = 10;
	LEVELSIZES[3] = 5;

	//Erstes Bit gibt jeweils an, ob Randknoten vorliegt!
	CELLSIZES[0] = 6;
	CELLSIZES[1] = 11;
	CELLSIZES[2] = 16;
	CELLSIZES[3] = 21;

	assert (numCells.size() == numLevel);
	vector<unsigned long long>(numNodes).swap(cellCoded);
	vector<unsigned long long>(numNodes).swap(matrixIndexCoded);
	for (int i = 0; i < numNodes; i++) {
		cellCoded[i] = 0LL;
		matrixIndexCoded[i] = 0LL;
	}

	LEVELOFFSETS.resize(numLevel+1);
	unsigned cellSum = 0;
	LEVELOFFSETS[0] = 0;
	for (int l = 0; l < numLevel; l++) {
		cellSum += numCells[l];
		LEVELOFFSETS[l+1] = cellSum;
	}
	matrixIndexVectors.resize(cellSum);
	for (unsigned i = 0; i < cellSum; i++) {
		matrixIndexVectors[i] = new vector<types::NodeID>;
	}
}


//Load Partition from filename
partition(string filename) {
	LEVELSIZES[0] = 20;
	LEVELSIZES[1] = 15;
	LEVELSIZES[2] = 10;
	LEVELSIZES[3] = 5;

	CELLSIZES[0] = 6;
	CELLSIZES[1] = 11;
	CELLSIZES[2] = 16;
	CELLSIZES[3] = 21;
	load(filename, true);
}


//Dummy-Constructor
partition() {
}


~partition() {
	for (int i = 0; i < matrixIndexVectors.size(); i++) {
		if (matrixIndexVectors[i] != NULL) {
			delete matrixIndexVectors[i];
			matrixIndexVectors[i] = NULL;
		}
	}
}


// Erhalte Zelle, in der sich v in Level l befindet
inline cellID getCellID(nodeID v, levelID l) {
	assert(l >= 0);
	//Shiften nach rechts...
	unsigned long long value = cellCoded[v];
	for (unsigned i = l; i > 0; i--) {
		value = (value >> LEVELSIZES[i-1]);
	}
	//... und linken Teil wegschneiden
	value = value & ((1LL << (LEVELSIZES[l])) - 1);
	return value;
}


// Erhalte Matrixindex von v in seiner Zelle in Level l
// bzw. -1, falls v nicht im Overlaylevel l existiert
inline int getMatrixIndex(nodeID v, levelID l)  {
	assert(l >= 0 && l < getNumLevel());
	//cout << "Shifting " << matrixIndexCoded[v] << endl;
	if (getMaxBorderLevel(v) < l)
		return -1;
	unsigned long long value = matrixIndexCoded[v];

	for (unsigned i = l; i > 0; i--) {
		value = (value >> CELLSIZES[i-1]);
	}
	// Border-Flag überspringen
	value = value & ((1LL << (CELLSIZES[l] - 1)) - 1);
	//cout << "Retreiving " << value << endl;
	return value;
}


// Umrechnung: Matrixindex --> NodeID in Graph
inline nodeID matrixIndexToNodeID(levelID l, cellID c, unsigned matrixIndex) {
	assert(l >= 0 && l < getNumLevel());
	assert(LEVELOFFSETS[l+1] - LEVELOFFSETS[l] >= c);
	return (*(matrixIndexVectors[LEVELOFFSETS[l] + c]))[matrixIndex];
}


// Be careful with this :)
inline vector<types::NodeID> *matrixIndexVector(levelID l, cellID c) {
	assert(l >= 0 && l < getNumLevel());
	assert(LEVELOFFSETS[l+1] - LEVELOFFSETS[l] >= c);
	return matrixIndexVectors[LEVELOFFSETS[l] + c];
}


// Erhalte das feinste Level, in dem sich u und v noch in der selben Zelle befinden
// Falls sich überhaupt keine gemeinsame Zelle findet => Gebe #Level zurück
inline levelID getLeastCommonLevel(nodeID u, nodeID v) {
	//Gemeinsamkeit erkennen
	unsigned long long value = ((cellCoded[u]) ^ (cellCoded[v]));
	//So lange nach rechts schieben, bis 0 dasteht
	int stepCount = 0;
	while ((stepCount < 4) && ((value & ((1LL << LEVELSIZES[stepCount]) - 1)) != 0)) {
		value = value >> LEVELSIZES[stepCount];
		stepCount++;
	}
	return stepCount;
}


// Erhalte maximales Level, in dem Knoten u noch Randknoten ist
// -1, falls u nirgends Randknoten ist
inline levelID getMaxBorderLevel(nodeID u) {
	unsigned long long value = matrixIndexCoded[u];
	levelID j = -1;
	// Checke Border-Flag
	while ((j+1 < this->getNumLevel()) && ((((value >> (CELLSIZES[j+1]-1)) & 1LL) == 1))) {
		j++;
		value = (value >> CELLSIZES[j]);
	}
	return j;
}


// Erhalte die Anzahl der Level dieser Partition
inline unsigned getNumLevel() {
	return LEVELOFFSETS.size() - 1;
}


// Erhalte die Anzahl Zellen eines Levels
inline unsigned getNumCells(levelID l) {
	assert(l >= 0);
	return LEVELOFFSETS[l+1] - LEVELOFFSETS[l];
}


// Erhalte die Anzahl der Randknoten einer Zelle
inline unsigned getCellBorderSize(levelID l, cellID c) {
	assert(l >= 0);
	return matrixIndexVectors[LEVELOFFSETS[l] + c]->size();
}


// Schmeisse v in Level l in Zelle c
void insert(nodeID v, levelID l, cellID c, bool isMatrixNode = false) {
	assert(l >= 0);
	//cout << "Adding node " << v << " in Cell " << c << " in Level " << l;
	//if (isMatrixNode) cout << " (matrixNode)";
	//cout << endl;
	if (isMatrixNode) {
		matrixIndexVectors[LEVELOFFSETS[l] + c]->push_back(v);
		// Update matrixIndex
		unsigned long long toAdd = getCellBorderSize(l,c)-1;
		unsigned long long borderFlag = 1LL;
		//cout << "matrixIndex: " << matrixIndexCoded[v] << " and toAdd: " << toAdd;
		for (unsigned i = l; i > 0; i--) {
			toAdd = (toAdd << CELLSIZES[i-1]);
			borderFlag = (borderFlag << CELLSIZES[i-1]);
		}
		borderFlag = (borderFlag << (CELLSIZES[l]-1));
		assert((toAdd & matrixIndexCoded[v]) == 0);
		matrixIndexCoded[v] += (toAdd + borderFlag);
	}
	// Just for the asserts...
	this->getMatrixIndex(v,l);
	// Update Cell
	unsigned long long toAdd = c;
	for (unsigned i = l; i > 0; i--) {
		toAdd = (toAdd << LEVELSIZES[i-1]);
	}
	assert((toAdd & cellCoded[v]) == 0);
	cellCoded[v] += toAdd;
}


// *** Serialization ***
void saveMatrixIndexVectors(const string filename, const bool verbose = true) {
	if (verbose) cout << "Writing matrixIndexVectors..." << endl;
	unsigned int size0 = matrixIndexVectors.size();

	if (verbose) cout << "Writing binary to " << filename << "... " << flush;
	ofstream f(filename.c_str(), ios::binary);
	if (!f) {
		if (verbose) cout << "Error opening " << filename << endl;
		return;
	}
	f.write(reinterpret_cast<const char*>(&size0), sizeof(size0));
	for(unsigned int i = 0; i < size0; i++) {
		unsigned int size1 = matrixIndexVectors[i]->size();
		f.write(reinterpret_cast<const char*>(&size1), sizeof(size1));
		for(unsigned int j = 0; j < size1; j++) {
			io::write_item(f, (*(matrixIndexVectors[i]))[j]);
		}
	}
	f.close();
	if (verbose) cout << "done!" << endl;
}


void loadMatrixIndexVectors(const string filename, const bool verbose = true) {
	if (verbose) cout << "Reading binary from " << filename << "... " << flush;
	// open binary file for reading
	ifstream in(filename.c_str(), ios::binary);
	if (!in) {
		if (verbose) cout << "Error opening " << filename << endl;
		return;
	}
	unsigned int size0;
	in.read(reinterpret_cast<char*>(&size0), sizeof(size0));
	matrixIndexVectors.resize(size0);
	for (unsigned int i = 0; i < size0; ++i) {
		unsigned int size1;
		in.read(reinterpret_cast<char*>(&size1), sizeof(size1));
		matrixIndexVectors[i] = new vector<types::NodeID>;
		matrixIndexVectors[i]->resize(size1);
		for (unsigned int j = 0; j < size1; ++j) {
			types::NodeID v;
			io::read_item(in, v);
			(*(matrixIndexVectors[i]))[j] = v;
		}
	}
	in.close();
	if (verbose) cout << "done!" << endl;
}


void save(const string filename, const bool verbose = true) {
	//Write cell coded
	if (verbose) cout << "Writing coded cell..." << endl;
	io::write_vector(cellCoded, filename + ".cell");
	//Write matrixIndex
	if (verbose) cout << "Writing Matrix Indices..." << endl;
	io::write_vector(matrixIndexCoded, filename + ".matrixIndex");
	if (verbose) cout << "Writing LevelOffsets..." << endl;
	io::write_vector(LEVELOFFSETS, filename + ".lO");
	saveMatrixIndexVectors(filename + ".mIV", verbose);
	if (verbose) cout << "Done writing Partition files." << endl;
}


void load(const string filename, const bool verbose = true) {
	//Read cell coded
	if (verbose) cout << "Reading Cell..." << endl;
	io::read_vector(cellCoded, filename + ".cell");
	//Read matrixIndex
	if (verbose) cout << "Reading Matrix Indices..." << endl;
	io::read_vector(matrixIndexCoded, filename + ".matrixIndex");
	if (verbose) cout << "Reading LevelOffsets..." << endl;
	io::read_vector(LEVELOFFSETS, filename + ".lO");
	loadMatrixIndexVectors(filename + ".mIV", verbose);
	if (verbose) cout << "Done Reading Partition files." << endl;
}


private:
// Zellen Codiert
// cellCoded[nodeID v] codiert Zelle von v in allen 4 Level
// Level 0:	20 Bit
// Level 1: 15 Bit
// Level 2: 10 Bit
// Level 3: 05 Bit
vector<unsigned long long> cellCoded;
unsigned LEVELSIZES[4];
// matrixIndex codiert
// Bem.: Matrix-Indizes sind 0-indiziert
// Erstes Bit gibt jeweils an, ob Randknoten vorliegt!
vector<unsigned long long> matrixIndexCoded;
unsigned CELLSIZES[4];
// matrixToNode
vector<vector<types::NodeID>*> matrixIndexVectors;
vector<unsigned> LEVELOFFSETS;
};
}
#endif /* PARTITIONH_H_ */
