/*
 * connection.h
 *
 *  Created on: Nov 2, 2010
 *      Author: pajor
 */

#ifndef CONNECTION_H_
#define CONNECTION_H_

#include <iostream>
#include <fstream>
#include <string>
#include <cassert>

using namespace std;


#include "../../typedefs.h"

namespace graphs {

class basicConnection {
public:

	inline static string id() {
		return "basicConnection 1";
	}

	inline types::TimePoint departure_time() const {
		return _dep;
	}
	inline types::EdgeWeight length() const {
		return _length;
	}
	inline bool valid() const {
		return _valid;
	}

	inline void set_departure_time(const types::TimePoint d) {
		_dep = d;
	}
	inline void set_length(const types::EdgeWeight l) {
		_length = l;
	}
	inline void set_valid() {
		_valid = true;
	}
	inline void unset_valid() {
		_valid = false;
	}


	// IO
	void write(ofstream &os) const {
		assert(os.is_open());
		os.write(reinterpret_cast<const char*>(this), sizeof(*this));
	}
	void read(ifstream &is) {
		assert(is.is_open());
		is.read(reinterpret_cast<char*>(this), sizeof(*this));
	}

	// Consistency stuff
	unsigned get_errors(const bool verbose = false) {
		unsigned num_errors(0);
		if (length() < 0) {
			if (verbose) cout << "ERROR: Connection has negative length: " << length() << "." << endl;
			++num_errors;
		}
/*		if (departure_time() >= types::PERIOD) {
			if (verbose) cout << "ERROR: Connection has departure time outside of period: " << departure_time() << " >= " << types::PERIOD << "." << endl;
			++num_errors;
		}*/
		return num_errors;
	}


	// Default initializer
	basicConnection() : _dep(0), _valid(false), _length(0) {}

private:
	types::TimePoint _dep : 31;
	bool _valid : 1;
	types::EdgeWeight _length;
};

}

#endif /* CONNECTION_H_ */
