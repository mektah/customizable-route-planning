/*
 * ptProperties.h
 *
 *  Created on: Nov 8, 2010
 *      Author: pajor
 */

#ifndef PTPROPERTIES_H_
#define PTPROPERTIES_H_


#include <iostream>
#include <fstream>
#include <string>
#include <cassert>

using namespace std;

#include "../../typedefs.h"
#include "basicProperties.h"


namespace graphs {

class ptProperties : public basicProperties {

public:

	inline static string id() {
		return "ptProperties 1";
	}

	std::size_t num_stations() const {
		return _stations;
	}

	void set_num_stations(const std::size_t s) {
		_stations = s;
	}

	// IO
	void write(ofstream &os) const {
		assert(os.is_open());
		os.write(reinterpret_cast<const char*>(this), sizeof(*this));
	}
	void read(ifstream &is) {
		assert(is.is_open());
		is.read(reinterpret_cast<char*>(this), sizeof(*this));
	}

private:

	std::size_t _stations;

};

}


#endif /* PTPROPERTIES_H_ */
