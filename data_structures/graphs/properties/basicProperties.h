/*
 * basicProperties.h
 *
 *  Created on: Nov 5, 2010
 *      Author: pajor
 */

#ifndef BASICPROPERTIES_H_
#define BASICPROPERTIES_H_


#include <iostream>
#include <fstream>
#include <string>
#include <cassert>

using namespace std;

#include "../../typedefs.h"

namespace graphs {

class basicProperties {

public:

	inline static string id() {
		return "basicProperties 1";
	}

	// IO
	void write(ofstream &os) const {
		assert(os.is_open());
		os.write(reinterpret_cast<const char*>(this), sizeof(*this));
	}
	void read(ifstream &is) {
		assert(is.is_open());
		is.read(reinterpret_cast<char*>(this), sizeof(*this));
	}
};

}


#endif /* BASICPROPERTIES_H_ */
