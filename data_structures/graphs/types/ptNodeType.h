/*
 * ptNodeType.h
 *
 *  Created on: Nov 8, 2010
 *      Author: pajor
 */

#ifndef PTNODETYPE_H_
#define PTNODETYPE_H_


namespace types {


enum ptNodeType {
	STATION_NODE = 0,
	ROUTE_NODE = 0,
	MIXED_NODE = 0,
};


}

#endif /* PTNODETYPE_H_ */
