/*
 * extheap.h
 *
 *  Created on: Nov 23, 2010
 *      Author: pajor
 */

#ifndef EXTHEAP_H_
#define EXTHEAP_H_

#include <vector>
using namespace std;

namespace pqueues {


// This is a "standard" heap element that is used with the ext heap. Please derive from this.
template<typename T>
class extkheap_element {

public:
	typedef T key_t;
	typedef int index_t;
	static const index_t NULLINDEX = 0xFFFFFFFF;

	inline T key() const { return _key; }
	inline index_t pos() const { return _pos; }
	inline void set_key(const T k) { _key = k; }
	inline void set_pos(const index_t p) { _pos = p; }

	extkheap_element() : _key(0), _pos(NULLINDEX) {}

private:
	T _key;
	index_t _pos;
};


template<int logK, typename E>
class extkheap {

public:

	// Typedefs of the heap
	typedef int size_t;
	typedef int index_t;
	typedef E element_t;
	typedef typename E::key_t T;

	static const index_t NULLINDEX = 0xFFFFFFFF;
	static const index_t K = 1 << logK;


public:

	// Constructor of the heap
	extkheap(size_t init_n = 100) : heap(0) {
		heap.reserve(init_n);
		init();
	}



	// Size of the heap
	inline size_t size() const {
		return cur_n;
	}

	// Heap empty?
	inline bool empty() const {
		return size() == 0;
	}

	// extract min element
	inline E* extractMin() {
		assert(!empty());

		// Get the front element
		E* front = heap[0];

		// Replace heap[0] by last element
		front->set_pos(NULLINDEX);
		--cur_n;
		if (!empty()) {
			heap[0] = heap[cur_n];
			heap[0]->set_pos(0);
			sift_down(0);
			heap.resize(cur_n);
		}

		return front;
	}

	// update an element of the heap
	inline void update(E* element, const T key) {
//		queue_element &cur_element = elements[element];

		if (element->pos() == NULLINDEX) {
			heap.resize(cur_n+1);
			heap[cur_n] = element;
			element->set_key(key);
			element->set_pos(cur_n);
			sift_up(cur_n++);
		} else {
			if (key > element->key()) {
				element->set_key(key);
				sift_down(element->pos());
			} else {
				element->set_key(key);
				sift_up(element->pos());
			}
		}
	}

	// Get min key
	inline T minKey() const {
		assert(!empty());
		return heap[0]->key();
	}

	// Get the key of an arbitrary element
	inline T key(E* element) const {
		assert(contains(element));
		return element->key();
	}

	// Clear the heap
	inline void reset() {
		init();
	}

	// Flush the heap
	inline void clear() {
		for (index_t i = 0; i < cur_n; ++i) {
			heap[i]->set_pos(NULLINDEX);
		}
		cur_n = 0;
		heap.resize(0);
	}

	// Whether an element is contained in the heap?
	inline bool contains(E* element) const {
		return element->pos() != -1;
	}


	// Dumps the heap into a stream.
	void dump(ostream &os) const {
		os << "Heap (keys): ";
		for (index_t i = 0; i < size(); ++i) {
			cout << "[" << i << ":" << heap[i]->key() << "] " << flush;
		}
		os << endl;
	}

	// Test for consistency
	bool is_consistent(const bool verbose = false) const {
		unsigned num_errors(0);

		// Do not have more elements than the size permits
		if (cur_n > heap.size()) {
			++num_errors;
			if (verbose) cout << "ERROR: The heap has max capacity of " << heap.size() << " and contains " << cur_n << " elements." << endl;
		}

		// Check heap property for all elements.
		index_t cur_i = 0;

		while (cur_i < cur_n) {
			index_t i_l = (cur_i << logK) + 1;
			index_t i_u = min(i_l + K, cur_n);
			for (index_t i = i_l; i < i_u; ++i) {
				if (heap[cur_i]->key() > heap[i]->key()) {
					++num_errors;
					if (verbose) {
						cout << "ERROR: Heap property violated. key(" << cur_i << ") = " << heap[cur_i]->key() << " > key(" << i << ") = " << heap[i]->key() << "." << endl;
						cout << "K: " << K << ", cur_i = " << cur_i << ", i_l = " << i_l << ", i_u = " << i_u << endl;
					}
				}
			}
			++cur_i;
		}

		return num_errors == 0;
	}


	// Return a description of this heap
	string description() {
		return "Heap w/ external storage";
	}




protected:

	// Reset the heap
	inline void init() {
		cur_n = 0;
	}

	// Sift up an element
	inline void sift_up(index_t i) {
		assert(i < cur_n);
		index_t cur_i = i;
		while (cur_i > 0) {
			index_t parent_i = (cur_i-1) >> logK;
			if (heap[parent_i]->key() > heap[cur_i]->key())
				swap(cur_i, parent_i);
			else
				break;
			cur_i = parent_i;
		}
	}

	// Sift down an element
	inline void sift_down(index_t i) {
		assert(i < cur_n);

		while (true) {
			index_t min_ind = i;
			T min_key = heap[i]->key();

			index_t child_ind_l = (i << logK) + 1;
			index_t child_ind_u = min(child_ind_l + K, cur_n);

			for (index_t j = child_ind_l; j < child_ind_u; ++j) {
				if (heap[j]->key() < min_key) {
					min_ind = j;
					min_key = heap[j]->key();
				}
			}

			// Exchange?
			if (min_ind != i) {
				swap(i, min_ind);
				i = min_ind;
			} else {
				break;
			}
		}
	}

	// Swap two elements in the heap
	inline void swap(const index_t i, const index_t j) {
//		E* el_i = heap[i];
//		E* el_j = heap[j];

		// Exchange positions
		heap[i]->set_pos(j);
		heap[j]->set_pos(i);

		// Exchange elements
		E* temp = heap[i];
		heap[i] = heap[j];
		heap[j] = temp;
	}



private:
	// Number of elements in the heap
	size_t cur_n;

	// A heap of elements
	vector<E*> heap;
};


}

#endif /* EXTHEAP_H_ */
