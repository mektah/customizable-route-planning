/*
 * kheap.h
 *
 *  Created on: Oct 20, 2010
 *      Author: pajor
 */

#ifndef KHEAP_H_
#define KHEAP_H_

#include <limits>
#include <iostream>
#include <cstring>
#include <cassert>

using namespace std;


namespace pqueues {


template<int logK, typename E, typename T>
class kheap {

public:

	// Typedefs of the heap
	typedef int size_t;
	typedef int index_t;

	static const index_t NULLINDEX = 0xFFFFFFFF;
	static const index_t K = 1 << logK;

	struct heap_element {
		T key;
		index_t element;
		heap_element() : key(0), element(0) {}
		heap_element(const T k, const index_t e) : key(k), element(e) {}
	};


public:

	// Constructor of the heap
	kheap(size_t n) : n(n), heap(NULL), pos(NULL) {
		pos = new index_t[n];
		heap = new heap_element[n];
		init();
	}

	// Destroy the heap
	~kheap() {
		if (pos != NULL)
			delete[] pos;
		if (heap != NULL)
			delete[] heap;
	}


	// Size of the heap
	inline size_t size() const {
		return cur_n;
	}

	// Heap empty?
	inline bool empty() const {
		return size() == 0;
	}

	// extract min element
	inline void extractMin(E &element, T &key) {
		assert(!empty());

		heap_element &front = heap[0];

		// Assign element and key
		element = front.element;
		key = front.key;

		// Replace heap[0] by last element
		pos[element] = NULLINDEX;
		--cur_n;
		if (!empty()) {
			front = heap[cur_n];
			pos[front.element] = 0;
			sift_down(0);
		}
	}

	// update am element of the heap
	inline void update(const E element, const T key) {
		if (pos[element] == NULLINDEX) {
			heap_element &back = heap[cur_n];
			back.key = key;
			back.element = element;
			pos[element] = cur_n;
			sift_up(cur_n++);
		} else {
			index_t el_pos = pos[element];
			heap_element &el = heap[el_pos];
			if (key > el.key) {
				el.key = key;
				sift_down(el_pos);
			} else {
				el.key = key;
				sift_up(el_pos);
			}
		}
	}

	// Get min key
	inline T minKey() const {
		assert(!empty());
		return heap[0].key;
	}

	// Get min element+key
	inline void min(E &element, T &key) {
		assert(!empty());
		key = heap[0].key;
		element = heap[0].element;
	}

	// Get the key of an arbitrary element
	inline T key(const E element) const {
		assert(contains(element));
		return heap[pos[element]].key;
	}

	// Clear the heap
	inline void reset() {
		init();
	}

	// Flush the heap
	inline void clear() {
		for (index_t i = 0; i < cur_n; ++i) {
			pos[heap[i].element] = NULLINDEX;
		}
		cur_n = 0;
	}

	// Whether an element is contained in the heap?
	inline bool contains(const E element) const {
		return pos[element] != -1;
	}


	// Dumps the heap into a stream.
	void dump(ostream &os) const {
		os << "Heap: ";
		for (index_t i = 0; i < size(); ++i) {
			cout << "[" << i << ":" << heap[i].key << "] " << flush;
		}
		os << endl;
		os << "Pos:  ";
		for (E e = 0; e < n; ++e) {
			if (pos[e] != NULLINDEX) {
				os << "[" << e << ":" << pos[e] << "] " << flush;
			}
		}
		os << endl;
	}

	// Test for consistency
	bool is_consistent(const bool verbose = false) const {
		unsigned num_errors(0);

		// Do not have more elements than the size permits
		if (cur_n > n) {
			++num_errors;
			if (verbose) cout << "ERROR: The heap has max size of " << n << " and contains " << cur_n << " elements." << endl;
		}

		// Check heap property for all elements.
		index_t cur_i = 0;

		while (cur_i < cur_n) {
			index_t i_l = (cur_i << logK) + 1;
			index_t i_u = min(i_l + K, cur_n);
			for (index_t i = i_l; i < i_u; ++i) {
				if (heap[cur_i].key > heap[i].key) {
					++num_errors;
					if (verbose) {
						cout << "ERROR: Heap property violated. key(" << cur_i << ") = " << heap[cur_i].key << " > key(" << i << ") = " << heap[i].key << "." << endl;
						cout << "K: " << K << ", cur_i = " << cur_i << ", i_l = " << i_l << ", i_u = " << i_u << endl;
					}
				}
			}
			++cur_i;
		}

		return num_errors == 0;
	}


	// Return a description of this heap
	string description() const {
		return "";//boost::lexical_cast<string>(K) + "-Heap";
	}




protected:

	// Reset the heap
	inline void init() {
		cur_n = 0;
		memset(pos, 0xFF, sizeof(index_t) * n);
		for (E i = 0; i < n; ++i) {
			assert(pos[i] == NULLINDEX);
		}
	}

	// Sift up an element
	inline void sift_up(index_t i) {
		assert(i < cur_n);
		index_t cur_i = i;
		while (cur_i > 0) {
			index_t parent_i = (cur_i-1) >> logK;
			if (heap[parent_i].key > heap[cur_i].key)
				swap(cur_i, parent_i);
			else
				break;
			cur_i = parent_i;
		}
	}

	// Sift down an element
	inline void sift_down(index_t i) {
		assert(i < cur_n);

		while (true) {
			index_t min_ind = i;
			T min_key = heap[i].key;

			index_t child_ind_l = (i << logK) + 1;
			index_t child_ind_u = std::min(child_ind_l + K, cur_n);

			for (index_t j = child_ind_l; j < child_ind_u; ++j) {
				if (heap[j].key < min_key) {
					min_ind = j;
					min_key = heap[j].key;
				}
			}

			// Exchange?
			if (min_ind != i) {
				swap(i, min_ind);
				i = min_ind;
			} else {
				break;
			}
		}
	}

	// Swap two elements in the heap
	inline void swap(const index_t i, const index_t j) {
		heap_element &el_i = heap[i];
		heap_element &el_j = heap[j];

		// Exchange positions
		pos[el_i.element] = j;
		pos[el_j.element] = i;

		// Exchange elements
		heap_element temp = el_i;
		el_i = el_j;
		el_j = temp;
	}



private:
	// Number of elements in the heap
	size_t cur_n;

	// Number of maximal elements
	size_t n;

	// Array of length heap_elements.
	heap_element *heap;

	index_t *pos;
};

}


#endif /* KHEAP_H_ */
