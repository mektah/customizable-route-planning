/*
 * singleLevelBucket.h
 *
 *  Created on: Oct 26, 2010
 *      Author: pajor
 */

#ifndef SINGLELEVELBUCKET_H_
#define SINGLELEVELBUCKET_H_


#include <numeric>

using namespace std;


#include "../typedefs.h"

namespace pqueues {

template<typename T>
class singleLevelBucket {

public:

	// Some types associated with this queue
	typedef unsigned element_t;
	typedef unsigned index_t;
	typedef unsigned size_t;

	const T NULLKEY;
	const index_t NULLINDEX;

	// This is a queue element
	struct queue_element {
		index_t prev;
		index_t next;
		T key;
	};


public:

	// Construct the queue
	singleLevelBucket(const size_t _n, const T _w) :
		NULLKEY(numeric_limits<T>::max()),
		NULLINDEX(numeric_limits<index_t>::max()),
		elements(NULL),
		buckets(NULL),
		wrap(_w),
		cur_n(0),
		n(_n),
		pointer(0)
	{
		buckets = new index_t[wrap];
		elements = new queue_element[n];
		init();
	}

	// Destroy the queue
	~singleLevelBucket() {
		if (buckets != NULL)
			delete[] buckets;
		if (elements != NULL)
			delete[] elements;
	}

	// Description of this queue type
	inline string description() const {
		return "Single Level Bucket Queue";
	}

	// Size of the queue
	inline size_t size() const {
		return cur_n;
	}

	// Heap empty?
	inline bool empty() const {
		return size() == 0;
	}

	// Is an element contained in the heap?
	inline bool contains(const element_t element) const {
		return elements[element].next != NULLINDEX;
	}


	// Reset the queue
	void reset() {
		init();
	}

	// Reset the queue mildly
	void clear() {
		// TODO: Implement
		while (!empty) {
			deleteMin();
		}
		pointer = 0;
		cur_n = 0;
	}


	// Insert or update an element
	inline void update(const element_t element, const T key) {
		if (contains(element)) {
			remove_element(element);
		}
		insert_element(element, key);
	}

	// Extract element with minimum key
	inline void extractMin(element_t &element, T &key) {
		assert(!empty());

		// Increase pointer (% wrap) until we hit a non-empty bucket
		while (buckets[pointer] == NULLINDEX) {
			++pointer;
			if (unlikely(pointer == wrap))
				pointer = 0;
		}

		element = buckets[pointer];
		key = elements[element].key;

		// Remove the element from the queue.
		remove_element(element);
	}

	// Delete an element (do not extract it)
	inline void deleteMin() {
		assert(!empty());

		// Increase pointer (% wrap) until we hit a non-empty bucket
		while (buckets[pointer] == NULLINDEX) {
			++pointer;
			if (unlikely(pointer == wrap))
				pointer = 0;
		}

		// Remove the element from the queue.
		remove_element(buckets[pointer]);
	}

	// Perform a consistency check
	bool is_consistent(const bool verbose = false) const {
		return true;
	}


	// Dumps the heap into a stream.
	void dump(ostream &os) const {
		os << endl;
	}


protected:

	// Initializes the queue
	void init() {
		memset(elements, 0xFF, sizeof(queue_element) * n);
		memset(buckets, 0xFF, sizeof(index_t) * wrap);
		pointer = 0;
		cur_n = 0;
	}


	// Removes an element from its linked list
	inline void remove_element(const element_t element) {
		assert(contains(element));

		// If the element is the last element of the list, remove it
		queue_element &el = elements[element];
		const T bucket = el.key % wrap;

		if (el.next == element) {
			assert(el.prev == element);
			buckets[bucket] = NULLINDEX;
		} else {
			elements[el.prev].next = el.next;
			elements[el.next].prev = el.prev;
			buckets[bucket] = el.prev;
		}

		--cur_n;
	}


	// Insert an element into the list.
	inline void insert_element(const element_t element, const T key) {
		queue_element &el = elements[element];
		const T bucket = key % wrap;
		element_t &felement = buckets[bucket];

		// Update key of element
		el.key = key;

		// Do pointer skewing
		if (felement == NULLINDEX) {
			el.next = element;
			el.prev = element;
			buckets[bucket] = element;
		} else {
			queue_element &fel = elements[felement];
			el.next = fel.next;
			el.prev = felement;
			elements[fel.next].prev = element;
			fel.next = element;
		}

		++cur_n;
	}


private:

	// This is an array of queue elements
	queue_element *elements;

	// This is the bucket structure
	index_t *buckets;

	// This is the maximum key (this queue is cyclic)
	T wrap;

	// This is the size of the queue
	size_t cur_n, n;

	// Pointer to the current bucket
	index_t pointer;
};


}

#endif /* SINGLELEVELBUCKET_H_ */
