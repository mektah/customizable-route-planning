/*
 * binaryheap.h
 *
 *  Created on: Dec 2, 2010
 *      Author: pajor
 *
 *  Code from Dominik Schultes.
 */

#ifndef BINARYHEAP_H_
#define BINARYHEAP_H_

#include <vector>
#include <cassert>
#include <limits>

using namespace std;

#include "../typedefs.h"

namespace pqueues {

struct EdgeWeightMeta {
	static const int MAX_VALUE;
};
const int EdgeWeightMeta::MAX_VALUE = std::numeric_limits<int>::max();


/**
 * In case of the (FIFO)BinaryHeap, we distinguish between an ExternalKey,
 * which is provided by the application (which uses the heap) and an internal Key,
 * which is used inside the heap. The ExternalKey is always a part of the internal Key.
 * A "key extractor" extracts the ExternalKey out of the internal Key.
 *
 * In the simple case of a BinaryHeap without the FIFO property, the internal Key just
 * corresponds to the ExternalKey. Thus, the SimpleKeyExtractor just returns the
 * given internal Key as the ExternalKey.
 * @param ExternalKey the type of the external key = the type of the internal key
 */
template < typename ExternalKey >
class SimpleKeyExtractor
{
public:
    /** Returns the given internal Key as the ExternalKey. */
    static ExternalKey key(ExternalKey k) {return k;}
};


/**
 * Represents an element of a BinaryHeap.
 * @param ExternalKey the type of the external key (see SimpleKeyExtractor)
 * @param Data the type of the application-specific data that is associated with this element
 * @param Count the type of the counter that counts the number of heap elements /
 *              the number of heap operations
 */
template < typename ExternalKey,
           typename Data,
           typename Count >
class BinaryHeapElement
{
public:
    /**
     * Constructor.
     * @param key key of the new element
     * @param index index of the new element within the heap
     */
    BinaryHeapElement(ExternalKey key, Count index) : _key(key), _index(index) {}


    /** Returns the external key of this element. */
    ExternalKey key() const {return _key;}

    /** Sets the external key of this element. */
    void key(ExternalKey newKey) {_key = newKey;}


    /** Returns a reference to the application-specific data object of this element. */
    Data& data() {return _data;}

    /** Returns a reference to the application-specific data object of this element. */
    const Data& data() const {return _data;}


    /** Marks that this element has been deleted from the heap. */
    void markDeleted() {index(0);}

    /** Returns true iff this element has been deleted from the heap. */
    bool hasBeenDeleted() const {return (_index==0);}


    /** Returns this element's index within the heap. */
    Count index() const {return _index;}

    /** Sets this element's index within the heap. */
    void index(Count newIndex) {_index = newIndex;}

private:
    /** The external key of this element. */
    ExternalKey _key;
    /** The application-specific data that is associated with this element. */
    Data _data;
    /**
     * This element's index within the heap.
     * 0 is used to indicate that this element has been deleted from the heap.
     */
    Count _index;
};

/**
 * Represents a binary heap.
 * @param ExternalKey the type of the external key (see SimpleKeyExtractor)
 * @param MetaExtKey class that provides data about the external key, e.g. the max value
 * @param Data the type of the application-specific data that is associated with this element
 * @param Count the type of the counter that counts the number of heap elements /
 *              the number of heap operations
 * @param Key the type of the internal key (see SimpleKeyExtractor)
 * @param KeyExtractor the "key extractor" that is used (see SimpleKeyExtractor)
 */
template < typename ExternalKey,
           typename MetaExtKey,
           typename Data,
           typename Count,
           typename Key = ExternalKey,
           typename KeyExtractor = SimpleKeyExtractor<ExternalKey> >
class BinaryHeap
{
public:
    /** The type of the elements stored in this heap. */
    typedef BinaryHeapElement<ExternalKey, Data, Count> PQElement;

    typedef Data PQData;

    /** Constructor. */
    BinaryHeap() {
        _elements.push_back( PQElement(ExternalKey(), 0) ); // add a dummy element
        _heap.push_back( IndexKey() ); // add a dummy element

        // changed by Reinhard Bauer:
        // change priority of dummy element in priority queue to allow the usage of negative priorities
        _heap[0].second=std::numeric_limits<Key>::min()+1;
    }

	virtual ~BinaryHeap()  {};

    /** Returns the size of this heap (= the number of elements). */
    Count size() const {
        return _heap.size()-1; // subtract dummy element (index 0)
    }

    /** Returns true iff the heap is empty. */
    bool empty() const { return (size() == 0); }

    /**
     * Returns the (external) key of the minimum element in this heap
     * or the maximum value of the ExternalKey data type iff the heap is empty.
     */
    ExternalKey min() const {
        if (empty()) return MetaExtKey::MAX_VALUE;
        return KeyExtractor::key(_heap[1].second);
    }

    /**
     * Creates a new element with the given (internal) key and inserts it into the heap.
     * @return the index of the new element.
     */
    Count insert(Key key) {
        Count element = _elements.size();
        Count index = _heap.size();
        _elements.push_back( PQElement(KeyExtractor::key(key), index) );
        _heap.push_back(IndexKey(element,key));
        upheap(index);
	//	if(_heap.size() < 100000)
	//  return element;
        return element;
    }

    /** Replaces a dummy element and inserts the actual element into the heap. */
    void insert(Key key, Count elementIndex) {
        PQElement& element = _elements[elementIndex];
        Count index = _heap.size();
        element.key(KeyExtractor::key(key));
        element.index(index);
        _heap.push_back(IndexKey(elementIndex,key));
        upheap(index);
    }

    /** Adds a dummy element, which is NOT inserted into the heap, yet. */
    void insertDummy() {
        _elements.push_back( PQElement(MetaExtKey::MAX_VALUE, 0) );
    }

    bool isDummy(Count element) const {
        return (_elements[element].key() == MetaExtKey::MAX_VALUE);
    }

    /** Returns a reference to the elements. */
    std::vector<PQElement>& elements() {return _elements;}

    /** Returns a reference to the elements. */
    const std::vector<PQElement>& elements() const {return _elements;}

    /**
     * Returns the index of the minimum element in this heap
     * or 0 iff the heap is empty.
     */
    Count minElement() const {
        if (empty()) return 0;
        return _heap[1].first;
    }

    /**
     * Deletes the minimum element in this heap.
     * Precondition: The heap is not empty.
     * @return the index of the deleted element
     */
    Count deleteMin() {
        assert( ! empty() );
        Count element = _heap[1].first;
        _heap[1] = _heap[_heap.size()-1];
        _heap.pop_back();
        Count index = 1;
        Count droppingElement = _heap[index].first;
        if (size() > 1) {
            // downheap:
            // Move the element at the top downwards
            // until the heap condition is restored.

            Key k = _heap[index].second;
            Count nextIndex = 2*index;
            while (nextIndex < _heap.size()) {
                if ( (nextIndex+1 < _heap.size()) &&
                      (_heap[nextIndex].second > _heap[nextIndex+1].second) )
                    nextIndex++;

				assert( _elements[_heap[nextIndex].first].key() == KeyExtractor::key(_heap[nextIndex].second) );

                if (k <= _heap[nextIndex].second) break;

                _heap[index] = _heap[nextIndex];
                _elements[_heap[nextIndex].first].index(index);
                index = nextIndex;
                nextIndex *= 2;
            }
            _heap[index].first = droppingElement;
            _heap[index].second = k;

            // end of downheap
        }
        _elements[droppingElement].index(index);

        _elements[element].markDeleted();
        return element;
    }

    /**
     * Deletes an arbitrary element in this heap.
     * Precondition: The heap is not empty.
     * @return the index of the deleted element
     */
    Count deleteArbitrary() {
        assert( ! empty() );
        Count element = _heap.back().first;
        _heap.pop_back();
        _elements[element].markDeleted();
        return element;
    }

    /**
     * Decreases the key of the given element:
     * sets the key to the given value.
     */
    void decreaseKey(Count element, Key newKey) {
        Count index = _elements[element].index();
        assert( index < _heap.size() );
        assert( _heap[index].first == element );

        _elements[element].key(KeyExtractor::key(newKey));
        _heap[index].second = newKey;
        upheap(index);
    }

    /**
     * Increases the key of the given element:
     * sets the key to the given value.
     */
    void increaseKey(Count element, Key newKey) {
        Count index = _elements[element].index();
        _elements[element].key(KeyExtractor::key(newKey));
        if (size() > 1) {
        	Count nextIndex = 2*index;
        	while (nextIndex < _heap.size()) {
        		if ( (nextIndex+1 < _heap.size()) &&
        				(_heap[nextIndex].second > _heap[nextIndex+1].second) )
        			nextIndex++;

        		if (newKey <= _heap[nextIndex].second) break;

        		_heap[index] = _heap[nextIndex];
        		_elements[_heap[nextIndex].first].index(index);
        		index = nextIndex;
        		nextIndex *= 2;
        	}
        	_heap[index].first = element;
        	_heap[index].second = newKey;
        }
        _elements[element].index(index);
    }


    void clear() {
      _heap.clear();
      _heap.push_back( IndexKey() ); // add a dummy element
      _elements.clear();
      _elements.push_back( PQElement(ExternalKey(), 0) ); // add a dummy element

        // changed by Reinhard Bauer:
        // change priority of dummy element in priority queue to allow the usage of negative priorities
        _heap[0].second=std::numeric_limits<Key>::min()+1;
    }

private:
    typedef std::pair<Count, Key> IndexKey;

    /**
     * The elements of this heap.
     * The order corresponds to the order of insertion and
     * is not changed by any heap operation. That implies
     * that an index of this vector can be used as a pointer
     * to the corresponding element.
     * It is possible that this vector contains holes (i.e.
     * dummy elements), namely for each element that has been
     * inserted into the other pqueue (for the other search direction)
     * but not to this one.
     * The first element (index 0) is a dummy element so that the index 0
     * can be used to mark elements that have not been inserted into the heap.
     */
    std::vector<PQElement> _elements;

    /**
     * "Pointers" (first) to the elements of this heap in the right heap order.
     * In addition (second), the internal key of the corresponding element.
     * The first element (index 0) is a dummy element so that the index 0
     * can be used to mark elements that have been deleted from the heap.
     */
    std::vector<IndexKey> _heap;




    /**
     * Move the element with the given index upwards
     * until the heap condition is restored.
     */
    void upheap(Count index) {
        Count risingElement = _heap[index].first;
        Key k = _heap[index].second;
        while (_heap[index / 2].second > k) {
            assert( index > 1 );
            assert( _elements[_heap[index / 2].first].key() == KeyExtractor::key(_heap[index / 2].second) );
            _heap[index] = _heap[index / 2];
            _elements[_heap[index].first].index(index);
            index /= 2;
        }
        _heap[index].first = risingElement;
        _heap[index].second = k;
        _elements[risingElement].index(index);
    }

};

}

#endif /* BINARYHEAP_H_ */
