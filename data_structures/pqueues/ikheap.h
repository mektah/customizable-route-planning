/*
 * ikheap.h
 *
 *  Created on: Oct 27, 2010
 *      Author: pajor
 */

#ifndef IKHEAP_H_
#define IKHEAP_H_

#include <limits>
#include <iostream>
#include <cstring>
#include <cassert>

using namespace std;

namespace pqueues {


template<int logK, typename E, typename T>
class ikheap {

public:

	// Typedefs of the heap
	typedef int size_t;
	typedef int index_t;

	static const index_t NULLINDEX = 0xFFFFFFFF;
	static const index_t K = 1 << logK;

	struct queue_element {
		T key;
		index_t pos;
		queue_element() : key(0), pos(NULLINDEX) {}
		queue_element(const T k, const index_t p) : key(k), pos(p) {}
	};


public:

	// Constructor of the heap
	ikheap(size_t n) : n(n), heap(NULL), elements(NULL) {
		elements = new queue_element[n];
		heap = new E[n];
		init();
	}

	// Destroy the heap
	~ikheap() {
		if (elements != NULL)
			delete[] elements;
		if (heap != NULL)
			delete[] heap;
	}


	// Size of the heap
	inline size_t size() const {
		return cur_n;
	}

	// Heap empty?
	inline bool empty() const {
		return size() == 0;
	}

	// extract min element
	inline void extractMin(E &element, T &key) {
		assert(!empty());

		// Get the front element
		E &front = heap[0];

		// Assign element and key
		element = front;
		key = elements[front].key;

		// Replace heap[0] by last element
		elements[front].pos = NULLINDEX;
		--cur_n;
		if (!empty()) {
			front = heap[cur_n];
			elements[front].pos = 0;
			sift_down(0);
		}
	}

	// update am element of the heap
	inline void update(const E element, const T key) {
		queue_element &cur_element = elements[element];

		if (cur_element.pos == NULLINDEX) {
			heap[cur_n] = element;
			cur_element.key = key;
			cur_element.pos = cur_n;
			sift_up(cur_n++);
		} else {
			if (key > cur_element.key) {
				cur_element.key = key;
				sift_down(cur_element.pos);
			} else {
				cur_element.key = key;
				sift_up(cur_element.pos);
			}
		}
	}

	// Get min key
	inline T minKey() const {
		assert(!empty());
		return elements[heap[0]].key;
	}

	// Get the key of an arbitrary element
	inline T key(const E element) const {
		assert(contains(element));
		return elements[element].key;
	}

	// Clear the heap
	inline void reset() {
		init();
	}

	// Flush the heap
	inline void clear() {
		for (index_t i = 0; i < cur_n; ++i) {
			elements[heap[i]].pos = NULLINDEX;
		}
		cur_n = 0;
	}

	// Whether an element is contained in the heap?
	inline bool contains(const E element) {
		return elements[element].pos != -1;
	}


	// Dumps the heap into a stream.
	void dump(ostream &os) const {
		os << "Heap (keys): ";
		for (index_t i = 0; i < size(); ++i) {
			cout << "[" << i << ":" << elements[heap[i]].key << "] " << flush;
		}
		os << endl;
		os << "Elements:  ";
		for (E e = 0; e < n; ++e) {
			if (elements[e].pos != NULLINDEX) {
				os << "[" << e << ":" << elements[e].pos << "] " << flush;
			}
		}
		os << endl;
	}

	// Test for consistency
	bool is_consistent(const bool verbose = false) const {
		unsigned num_errors(0);

		// Do not have more elements than the size permits
		if (cur_n > n) {
			++num_errors;
			if (verbose) cout << "ERROR: The heap has max size of " << n << " and contains " << cur_n << " elements." << endl;
		}

		// Check heap property for all elements.
		index_t cur_i = 0;

		while (cur_i < cur_n) {
			index_t i_l = (cur_i << logK) + 1;
			index_t i_u = min(i_l + K, cur_n);
			for (index_t i = i_l; i < i_u; ++i) {
				if (elements[heap[cur_i]].key > elements[heap[i]].key) {
					++num_errors;
					if (verbose) {
						cout << "ERROR: Heap property violated. key(" << cur_i << ") = " << elements[heap[cur_i]].key << " > key(" << i << ") = " << elements[heap[i]].key << "." << endl;
						cout << "K: " << K << ", cur_i = " << cur_i << ", i_l = " << i_l << ", i_u = " << i_u << endl;
					}
				}
			}
			++cur_i;
		}

		return num_errors == 0;
	}


	// Return a description of this heap
	string description() {
		return "K-Heap";
	}




protected:

	// Reset the heap
	inline void init() {
		cur_n = 0;
		memset(elements, 0xFF, sizeof(queue_element) * n);
	}

	// Sift up an element
	inline void sift_up(index_t i) {
		assert(i < cur_n);
		index_t cur_i = i;
		while (cur_i > 0) {
			index_t parent_i = (cur_i-1) >> logK;
			if (elements[heap[parent_i]].key > elements[heap[cur_i]].key)
				swap(cur_i, parent_i);
			else
				break;
			cur_i = parent_i;
		}
	}

	// Sift down an element
	inline void sift_down(index_t i) {
		assert(i < cur_n);

		index_t cur_i = i;

		while (true) {
			index_t min_ind = i;
			T min_key = elements[heap[i]].key;

			index_t child_ind_l = (i << logK) + 1;
			index_t child_ind_u = min(child_ind_l + K, cur_n);

			for (index_t j = child_ind_l; j < child_ind_u; ++j) {
				if (elements[heap[j]].key < min_key) {
					min_ind = j;
					min_key = elements[heap[j]].key;
				}
			}

			// Exchange?
			if (min_ind != i) {
				swap(i, min_ind);
				i = min_ind;
			} else {
				break;
			}
		}
	}

	// Swap two elements in the heap
	inline void swap(const index_t i, const index_t j) {
		E &el_i = heap[i];
		E &el_j = heap[j];

		// Exchange positions
		elements[el_i].pos = j;
		elements[el_j].pos = i;

		// Exchange elements
		E temp = el_i;
		el_i = el_j;
		el_j = temp;
	}



private:
	// Number of elements in the heap
	size_t cur_n;

	// Number of maximal elements
	size_t n;

	// A heap of elements
	E *heap;

	// Elements in the queue.
	queue_element *elements;
};

}

#endif /* IKHEAP_H_ */
