CPP=g++

FLAGS=-O3 -fomit-frame-pointer -funroll-loops -fopenmp -Wall -pedantic

all: blatt1 blatt2

blatt1:
	${CPP} ${FLAGS} -o blatt1 blatt1.cpp

blatt2:
	${CPP} ${FLAGS} -o blatt2 blatt2.cpp
	
clean:
	rm blatt1 blatt2
