// Erstes Blatt
//
// Bitte auf der Kommandozeile `make blatt1` eingeben zum Kompilieren.


#include <iostream>
using namespace std;


// Rudiment�res Parsen von Command-Line-Options
#include "tools/commandLineParser.h"

// Einbinden von Graphen
#include "data_structures/graphs.h"




int main(int argn, char **argv) {

	// Kommandozeilenparser instanziieren.
	tools::commandLineParser clp(argn, argv);


	// Wenn kein Graph angegeben wurde, mit einem Fehler abbrechen.
	if (!clp.isSet("g")) {
		cout << "Kein Graph angegeben. Bitte mit `" << argv[0] << " -g <graph>` aufrufen." << endl;
		return 0;
	}

	// Graph-Dateinamen von der Kommandozeile einlesen.
	const string graphFilename = clp.value<string>("g", "");

	// Laden des graphen
	graphs::roadGraph graph(graphFilename);



	// Statistiken Berechnen.
	cout << "Berechne Statistiken... " << flush;

	cout << "fertig." << endl;


	// Ausgeben der Statistiken.
	cout << "Statistiken: " << endl;


	return 0;
}
