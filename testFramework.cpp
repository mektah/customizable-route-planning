// *** Helpful tool for many testing purposes //

#include <string>
#include <iostream>
#include <cstdlib>
#include <list>
#include <math.h>
using namespace std;
#include "tools/commandLineParser.h"
#include "data_structures/graphs.h"
#include "data_structures/graphs/partition.h"
#include "algorithms/customizable.h"
#include "algorithms/bidijkstra.h"
#include "algorithms/dijkstra.h"
// Netter Fortschrittsbalken
#include "tools/progressBar.h"
#include "data_structures/graphs/metrics/metric.h"
#include "geometry/coordinates.h"
#include "geometry/pathVisualizer.h"
#include "tools/pinThreads.h"

static const string ERR_INPUT = "Ungültige Eingabe";
static const string ERR_CONSISTENCY = "Fehlerhafter Wert eingegeben";
static const string kmlFilename = "out.kml";

void runRandomQueries(graphs::roadGraph &g, algorithms::customizable<graphs::roadGraph> &algorithm, unsigned numQueries, int randomSeed);
void runQuery(graphs::roadGraph &g, algorithms::customizable<graphs::roadGraph> &algorithm, types::NodeID s, types::NodeID t, bool extractPath, geometry::coordinates &coord);
void addMetric(algorithms::customizable<graphs::roadGraph> &algorithm, string metricFilename);
void createMetric(graphs::roadGraph &g, algorithms::customizable<graphs::roadGraph> &algorithm, unsigned metricChoice, int randomSeed, geometry::coordinates *coord = NULL);
void changeMetric(algorithms::customizable<graphs::roadGraph> &algorithm, types::MetricID m);
void setNumPhantomLevel(algorithms::customizable<graphs::roadGraph> &algorithm, unsigned n);

int main(int argn, char **argv) {
	tools::pinThread(0);

	// Kommandozeilenparser instanziieren.
	tools::commandLineParser *clp = new tools::commandLineParser(argn, argv);
	// Wenn kein Graph angegeben wurde, mit einem Fehler abbrechen.
	if (!clp->isSet("g")) {
		cout << "Kein Graph angegeben. Bitte mit `" << argv[0] << " -g <graph>` aufrufen." << endl;
		return 0;
	}
	// Wenn keine Partition angegeben wurde, mit einem Fehler abbrechen.
	if (!clp->isSet("p")) {
		cout << "Keine Partition angegeben. Bitte mit `" << argv[0] << " -p <partition>` aufrufen." << endl;
		return 0;
	}
	const int randomSeed = clp->value<int>("s", 31101982); // Saat f�r den Zufallszahlengenerator
	// Graph-Dateinamen von der Kommandozeile einlesen.
	const string graphFilename = clp->value<string>("g", "");
	const string partFilename = clp->value<string>("p", "");
	const string coordFilename = clp->value<string>("c", "");
	delete clp;

	graphs::roadGraph g(graphFilename);
	graphs::partition p(partFilename);
	algorithms::customizable<graphs::roadGraph> algorithm(&g, &p, 0);
	//algorithms::bidijkstra<graphs::roadGraph> bidijkstra(g);

	geometry::coordinates coord;
	if (coordFilename != "") {
		coord.load(coordFilename);
	}

	int menuChoice;
	bool running = true;
	while(running) {
		cout << endl << "Please select:\n"
			 << "1 - Run random queries\n"
			 << "2 - Run s-t-query\n"
			 << "3 - Load metric from file\n"
			 << "4 - Create new metric\n"
			 << "5 - Change current metric\n"
			 << "6 - Set number of phantom-levels used (default: 0)\n"
			 << "7 - Exit\n"
			 << "> " << flush;
		cin >> menuChoice;
		switch(menuChoice) {
		case 1: {
			unsigned n(0);
			cout << endl << "How many runs? > " << flush;
			cin >> n;
			runRandomQueries(g, algorithm, n, randomSeed);
			break;
		} case 2: {
			types::NodeID s,t = types::NULLNODE;
			char path;
			cout << endl << "s >" << flush;
			cin >> s;
			cout << endl << "t >" << flush;
			cin >> t;
			cout << endl << "Export path to file? (y/n) >" << flush;
			cin >> path;
			runQuery(g, algorithm, s, t, path == 'y' || path == 'Y', coord);
			break;
		} case 3: {
			string metricFilename;
			cout << endl << "Filename? >";
			getline(cin, metricFilename);
			addMetric(algorithm, metricFilename);
			break;
		} case 4: {
			unsigned metricChoice(0);
			cout << endl << "Please select the type:\n"
			     << "1 - Random metric\n"
			     << "2 - Unity metric\n"
			     << "3 - Distance metric\n"
			     << "> " << flush;
			cin >> metricChoice;
			createMetric(g, algorithm, metricChoice, randomSeed, metricChoice == 3 ? &coord : NULL);
			break;
		} case 5: {
			types::MetricID m(0);
			cout << endl << "MetricID? (Type 0 for original metric of the graph) >" << flush;
			cin >> m;
			changeMetric(algorithm, m);
			break;
		} case 6: {
			unsigned n(0);
			cout << endl << "Number of phantom levels? >" << flush;
			cin >> n;
			setNumPhantomLevel(algorithm, n);
			break;
		} case 7: {
			running = false;
			break;
		} default:
			cerr << endl << ERR_INPUT;
			break;
		}
	}
	return 0;
}

void runRandomQueries(graphs::roadGraph &g, algorithms::customizable<graphs::roadGraph> &algorithm, unsigned numQueries, int randomSeed) {
	tools::progressBar bar(numQueries);
	double elapsed(0);
	long checksum(0);
	srand(randomSeed);
	cout << endl << "Running " << numQueries << " random queries: " << flush;
	for (int i = 0; i < numQueries; ++i) {
			// Berechnen der Start- und Zielknoten
			types::NodeID source_node = rand() % g.get_num_nodes();
			types::NodeID target_node = rand() % g.get_num_nodes();
			if (source_node == target_node) {
				--i;
				continue;
			}
			// Aufruf von unserem Algorithmus
			algorithm.run(source_node, target_node);
			elapsed += algorithm.get_elapsed_milliseconds();
			checksum += algorithm.get_distance();

			++bar;
		}
	cout << "done!\n"
		 << "Statistics:\n"
		 << "\tChecksum: " << checksum << endl
		 << "\tAverage running time: " << elapsed / double(numQueries) << " ms\n";
}

void runQuery(graphs::roadGraph &g, algorithms::customizable<graphs::roadGraph> &algorithm, types::NodeID s, types::NodeID t, bool extractPath, geometry::coordinates &coord) {
	if (s >= g.get_num_nodes() || t >= g.get_num_nodes()) {
		cerr << endl << ERR_CONSISTENCY;
		return;
	}
	algorithm.run(s,t);
	cout << endl << "Found shortest path of length " << algorithm.get_distance() << flush;
	if (extractPath) {
		if (coord.size() != g.get_num_nodes()) {
			cerr << endl << "Fehler: Ungültige oder keine Koordinaten-Datei gesetzt. Bitte mit -c <coordFilename> aufrufen." << flush;
			return;
		}
		geometry::pathVisualizer v(&coord);
		list<types::NodeID> connection;
		list<types::LevelID> levels;
		connection.clear();
		algorithm.unpackConnection(connection, levels);
		v.visualize(connection, kmlFilename);
		cout << endl << "Extracted path to " << kmlFilename << endl;
	}
}

void addMetric(algorithms::customizable<graphs::roadGraph> &algorithm, string metricFilename) {
	//TODO: new wieder zerstören, falls Metrik gelöscht wird
	graphs::metric<graphs::roadGraph> *M = new graphs::metric<graphs::roadGraph>(metricFilename);
	types::MetricID m = algorithm.addMetric(M);
	cout << endl << "Added metric with ID " << m << endl;;
}

void createMetric(graphs::roadGraph &g, algorithms::customizable<graphs::roadGraph> &algorithm, unsigned metricChoice, int randomSeed, geometry::coordinates *coord) {
	if (metricChoice > 3) {
		cerr << endl << ERR_INPUT;
		return;
	}
	srand(randomSeed);
	//TODO: new wieder zerstören, falls Metrik gelöscht wird
	// +1 wegen Dummy-Edge am Schluss
	//TODO: Sonderfallbehandlung nötig für letzte Kante?
	graphs::metric<graphs::roadGraph> *M = new graphs::metric<graphs::roadGraph>(g.get_num_edges() + 1);

	types::NodeID u;
	graphs::roadGraph::edgeType *e;
	types::EdgeWeight w = types::INFTY;
	FORALL_EDGES(g, u, e) {
		if (metricChoice == 1) {
			w = rand() % 1000;
		} else if (metricChoice == 2) {
			w = 1;
		} else if (metricChoice == 3) {
			assert(coord != NULL);
			const geometry::latLng p1 = (*coord)[u];
			const geometry::latLng p2 = (*coord)[e->node()];
			w = floor(coord->get_distance_haversine(p1, p2) + 0.5);
		} else {
			assert(false);
		}
		M->setWeight(g.get_edge_id(e), w);
	}
	types::MetricID m = algorithm.addMetric(M);
	cout << endl << "Added metric with ID " << m << endl;;
}

void changeMetric(algorithms::customizable<graphs::roadGraph> &algorithm, types::MetricID m)  {
	//TODO: Fehlerhafte Eingaben abfangen?
	algorithm.setMetric(m);
}

void setNumPhantomLevel(algorithms::customizable<graphs::roadGraph> &algorithm, unsigned n) {
	//TODO: Fehlerhafte Eingaben abfangen?
	algorithm.setNumPhantomLevel(n);
}
