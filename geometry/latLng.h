/*
 * latLng.h
 *
 *  Created on: Nov 10, 2010
 *      Author: pajor
 */

#ifndef LATLNG_H_
#define LATLNG_H_


namespace geometry {

struct latLng {
	double lat;
	double lng;
	latLng() : lat(0), lng(0) {}
	latLng(double lat_, double lng_) : lat(lat_), lng(lng_) {}

	latLng operator+(const latLng ll) const {
		return latLng(lat + ll.lat, lng + ll.lng);
	}

	bool operator==(const latLng &other) const {
		return (this->lat == other.lat && this->lng == other.lng);
	}
};

}


#endif /* LATLNG_H_ */
