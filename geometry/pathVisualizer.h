// Dijkstra's Algorithmus
#ifndef PATHVISUALIZER_H_
#define PATHVISUALIZER_H_

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include "coordinates.h"
#include "latLng.h"
using namespace std;

#include "../tools/timer.h"
#include "../data_structures/typedefs.h"

namespace geometry {
class pathVisualizer {

public:

// Construct this class
pathVisualizer(geometry::coordinates *c) {
	this->c = c;
}
void visualize(list<types::NodeID> &connection, string targetFilename, bool verbose = true) {
	ofstream o;
	o.open(targetFilename.c_str());

	o << "<kml>" << endl
	  << "<Document>" << endl
	  << "<name>Path Visualization</name>"
	  << "<Placemark>" << endl
	  << "<LineString>" << endl
	  << "<extrude>1</extrude><tessellate>1</tessellate>" << endl
	  << "<altitudeMode>absolute</altitudeMode>" << endl
	  << "<coordinates>" << endl;
	 types::nodeContainer::iterator it;

	for (it = connection.begin(); it != connection.end(); it ++) {
		geometry::latLng l = (*c)[*it];
		o << l.lng << "," << l.lat << endl;
	}

	o << "</coordinates>" << endl
	  << "</LineString>" << endl
	  << "</Placemark>" << endl
	  << "</Document>" << endl
	  << "</kml>" << endl;

	o.close();
}

private:

geometry::coordinates *c;

};
}
#endif
