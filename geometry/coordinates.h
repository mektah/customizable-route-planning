/*
 * coordinates.h
 *
 *  Created on: Nov 10, 2010
 *      Author: pajor
 */

#ifndef COORDINATES_H_
#define COORDINATES_H_

#include <vector>
#include <math.h>
using namespace std;

#include "latLng.h"
#include "../io/container_io.h"


namespace geometry {

class coordinates {

public:

	typedef std::size_t size_t;

public:


	// Construct the object
	coordinates() : latlngs(0) {}
	coordinates(const size_t s) : latlngs(s) {}

	// Access elements
	inline latLng &operator[](const size_t i) {
		return latlngs[i];
	}

	// Return the number of coordinates in this file.
	inline size_t size() const {
		return latlngs.size();
	}

	// Return if this thing is empty.
	inline bool empty() const {
		return latlngs.empty();
	}

	// Resize the latlngs
	inline void resize(const size_t s) {
		latlngs.resize(s);
	}

	// Load coordinates from file.
	void load(const string filename, const bool verbose = true) {
		io::read_vector(latlngs, filename, verbose);
	}

	// Save coordinates to file
	void save(const string filename, const bool verbose = true) const {
		io::write_vector(latlngs, filename, verbose);
	}

	// Compute the distance between two latlongs using the Haversine formula.
	// Output is in meters.
	// Taken from: http://www.movable-type.co.uk/scripts/latlong.html
	double get_distance_haversine(const geometry::latLng p1, const geometry::latLng p2) {
		const double EarthRadiusInMeters = 6372797.560856;
		const double DegToRad = 0.017453292519943295769236907684886;
		const double diffLat = (p2.lat - p1.lat) * DegToRad;
		const double diffLon = (p2.lng - p1.lng) * DegToRad;
		const double lat1 = p1.lat * DegToRad;
		const double lat2 = p2.lat * DegToRad;

		const double a = sin(diffLat / 2.0) * sin(diffLat / 2) +
	        sin(diffLon / 2.0) * sin(diffLon / 2.0) * cos(lat1) * cos(lat2);
		const double c = 2.0 * atan2(sqrt(a), sqrt(1-a));
		return EarthRadiusInMeters * c;
	}


private:

	// This is a vector of coordinates.
	vector<latLng> latlngs;
};

}

#endif /* COORDINATES_H_ */
