// Zweites Blatt
//
// Bitte auf der Kommandozeile `make blatt2` eingeben zum Kompilieren.


#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;


// Rudiment�res Parsen von Command-Line-Options
#include "tools/commandLineParser.h"

// Einbinden von Graphen
#include "data_structures/graphs.h"

// Einbinden von Dijkstra's Algorithmus
#include "algorithms/dijkstra.h"
#include "algorithms/bidijkstra.h"

// Netter Fortschrittsbalken
#include "tools/progressBar.h"

int main(int argn, char **argv) {

	// Kommandozeilenparser instanziieren.
	tools::commandLineParser clp(argn, argv);


	// Wenn kein Graph angegeben wurde, mit einem Fehler abbrechen.
	if (!clp.isSet("g")) {
		cout << "Kein Graph angegeben. Bitte mit `" << argv[0] << " -g <graph>` aufrufen." << endl;
		return 0;
	}

	// Graph-Dateinamen von der Kommandozeile einlesen.
	const string graphFilename = clp.value<string>("g", "");
	const int numQueries = clp.value<int>("n", 100);
	const int randomSeed = clp.value<int>("s", 31101982); // Saat f�r den Zufallszahlengenerator

	// Laden des graphen
	graphs::roadGraph graph(graphFilename);

	// Instanziierung von Dijkstra's Algorithmus
	algorithms::dijkstra<graphs::roadGraph> algorithm(graph);

	// Random-Queries ausf�hren
	cout << endl << "Running " << numQueries << " random queries: " << flush;
	tools::progressBar bar(numQueries);

	srand(randomSeed);
	long checksum(0);
	unsigned scannedNodes(0), relaxedEdges(0);
	double elapsed(0.0);

	for (int i = 0; i < numQueries; ++i) {
		// Berechnen der Start- und Zielknoten
		types::NodeID source_node = rand() % graph.get_num_nodes();
		types::NodeID target_node = rand() % graph.get_num_nodes();

		if (source_node == target_node) {
			--i;
			continue;
		}

		// Aufruf von Dijkstra's algorithmus
		algorithm.run(source_node, target_node);

		cout << algorithm.get_distance(target_node) << endl;

		// Akkumulieren der Statistiken.
		checksum += algorithm.get_distance(target_node);
		scannedNodes += algorithm.get_num_scanned_nodes();
		relaxedEdges += algorithm.get_num_relaxed_edges();
		elapsed += algorithm.get_elapsed_milliseconds();
		
		// Ausgabe von Debugzeugs
		//cout << "Distanz: " << algorithm.get_distance(target_node) << "\n";

		// Fortschrittsbalken erh�hen
		++bar;
	}
	cout << " done." << endl << endl;

	// Ausgabe der Ergebnisse.
	cout << "Results: " << endl
	     << "\tChecksum: " << checksum << endl
	     << "\tAverage running time: " << elapsed / double(numQueries) << " ms" << endl
	     << "\tAverage search space: " << scannedNodes / numQueries << endl
	     << "\tAverage relaxed edges: " << relaxedEdges / numQueries << endl;


	return 0;
}
