#include <iostream>
using namespace std;


#include "data_structures/graphs/overlay.h"
#include "data_structures/graphs.h"




int main(int argn, char **argv) {

	// Bastle Testgraph
	graphs::roadGraph G;
	G.add_nodes(6);

	graphs::roadGraph::edgeType *e(NULL);
	e = G.add_edge(0,1);
	e->set_forward();
	e = G.add_edge(0,5);
	e->set_forward();
	e = G.add_edge(1,0);
	e->set_forward();
	e = G.add_edge(2,1);
	e->set_forward();
	e = G.add_edge(3,1);
	e->set_forward();
	e = G.add_edge(3,2);
	e->set_forward();
	e = G.add_edge(4,3);
	e->set_forward();
	e = G.add_edge(4,0);
	e->set_forward();
	e = G.add_edge(5,4);
	e->set_forward();

	e = G.add_edge(1,0);
	e->set_backward();
	e = G.add_edge(5,0);
	e->set_backward();
	e = G.add_edge(0,1);
	e->set_backward();
	e = G.add_edge(1,2);
	e->set_backward();
	e = G.add_edge(1,3);
	e->set_backward();
	e = G.add_edge(3,1);
	e->set_backward();
	e = G.add_edge(3,4);
	e->set_backward();
	e = G.add_edge(0,4);
	e->set_backward();
	e = G.add_edge(4,5);
	e->set_backward();

	G.save("test.gr");

	return 0;
}
