// TEST-Datei, um customizable zu kompilieren

#include <string>
#include <iostream>
#include <list>
#include <cstdlib>
using namespace std;

#include "tools/commandLineParser.h"
#include "data_structures/graphs.h"
#include "data_structures/graphs/partition.h"
#include "algorithms/customizable.h"
#include "algorithms/bidijkstra.h"
#include "algorithms/dijkstra.h"
// Netter Fortschrittsbalken
#include "tools/progressBar.h"


int main(int argn, char **argv) {

	// Kommandozeilenparser instanziieren.
	tools::commandLineParser clp(argn, argv);

	// Wenn kein Graph angegeben wurde, mit einem Fehler abbrechen.
	if (!clp.isSet("g")) {
		cout << "Kein Graph angegeben. Bitte mit `" << argv[0] << " -g <graph>` aufrufen." << endl;
		return 0;
	}

	// Wenn keine Partition angegeben wurde, mit einem Fehler abbrechen.
	if (!clp.isSet("p")) {
		cout << "Keine Partition angegeben. Bitte mit `" << argv[0] << " -p <partition>` aufrufen." << endl;
		return 0;
	}

	// Graph-Dateinamen von der Kommandozeile einlesen.
	const string graphFilename = clp.value<string>("g", "");
	const string partFilename = clp.value<string>("p", "");
	const int numQueries = clp.value<int>("n", 100);
	const int randomSeed = clp.value<int>("s", 31101982); // Saat f�r den Zufallszahlengenerator
	const int numPhantomLevel = clp.value<int>("l", 0);

	graphs::roadGraph g(graphFilename);
	graphs::partition p(partFilename);

	// Instanziierung von Customizable
	algorithms::customizable<graphs::roadGraph> algorithm(g, p, numPhantomLevel);
	//algorithms::bidijkstra<graphs::roadGraph> algorithm(g);

	// Random-Queries ausführen
	cout << endl << "Running " << numQueries << " random queries: " << flush;
	tools::progressBar bar(numQueries);
	srand(randomSeed);
	long checksum(0);
	unsigned scannedNodes(0), relaxedEdges(0);
	double elapsed(0.0);
	list<types::NodeID> connection;
	list<types::LevelID> levels;
	list<types::NodeID>::iterator it;

	for (int i = 0; i < numQueries; ++i) {
		// Berechnen der Start- und Zielknoten
		types::NodeID source_node = rand() % g.get_num_nodes();
		types::NodeID target_node = rand() % g.get_num_nodes();

		if (source_node == target_node) {
			--i;
			continue;
		}

		// Aufruf von Dijkstra's algorithmus
		int distance = algorithm.run(source_node, target_node);
		//cout << distance << endl;

		// Akkumulieren der Statistiken.
		checksum += distance;
		scannedNodes += algorithm.get_num_scanned_nodes();
		relaxedEdges += algorithm.get_num_relaxed_edges();
		elapsed += algorithm.get_elapsed_milliseconds();

		//Auspacken der Verbindung
		algorithm.unpackConnection(connection, levels);

		//for (it = connection.begin(); it != connection.end(); it++) {
		//	cout << "Knoten ist " << *it << " mit MaxBorderLevel " << p.getMaxBorderLevel(*it) <<  endl;
		//}

		// Fortschrittsbalken erhöhen
		++bar;
	}
	cout << " done." << endl << endl;

	// Ausgabe der Ergebnisse.
	cout << "Results: " << endl
	     << "\tChecksum: " << checksum << endl
	     << "\tAverage running time: " << elapsed / double(numQueries) << " ms" << endl
	     << "\tAverage search space: " << scannedNodes / numQueries << endl
	     << "\tAverage relaxed edges: " << relaxedEdges / numQueries << endl;

	return 0;
}
