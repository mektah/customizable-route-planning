#pragma once


#include <cassert>
#include <iostream>
#include <ostream>
using namespace std;


namespace tools {

class progressBar {

public:

	progressBar(const int n, const bool v = true, ostream &o = cout) : verbose(v), os(o) {
		init(n);
	}

	void init(int n) {
		numSteps          = n;
		stepsDone         = 0;
		lastDrawnPercent = 0;
		if (verbose) os << "0% " << flush;
	}

	void iterate() {
		++stepsDone;
		unsigned int until = (stepsDone*100) / numSteps;
		draw(until);
	}

	void iterateTo(int target) {
		assert(target >= stepsDone);
		stepsDone = target;
		unsigned int until = (stepsDone*100) / numSteps;
		draw(until);
	}

	void operator++() {
		iterate();
	}


protected:

	inline void draw(unsigned int until) {
		for (unsigned short i = (lastDrawnPercent+1); i <= until; ++i) {
			if (i % 20 == 0) {
				if (i > 0 && verbose)
					os << " " << i << "% " << std::flush;
			} else {
				if (i % 5 == 0 && verbose)
					os << "." << flush;
			}
		}
		lastDrawnPercent = until;
	}


private:
	bool verbose;
	ostream &os;
	int numSteps;
	int stepsDone;
	unsigned short lastDrawnPercent;
	double percentDone;
};


}
