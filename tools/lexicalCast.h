#pragma once

#include <string>
#include <sstream>
using namespace std;


namespace tools {

// Cast stuff
template<typename U, typename T>
U lexicalCast(const T in) {
	stringstream ss;
	ss << in;
	U out;
	ss >> out;
	return out;
}


}
