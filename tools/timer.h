/*
 * timer.h
 *
 *  Created on: Nov 23, 2010
 *      Author: pajor
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <sys/time.h>

namespace tools {

class timer
{
public:
	timer() : start(timestamp()) {}

	void restart() {
		start = timestamp();
	}

	double elapsed() {
		double cur = timestamp();
		return cur - start;
	}

private:
	double start;

	/** Returns a timestamp ('now') in miliseconds (incl. a fractional part). */
	inline double timestamp() {
		timeval tp;
		gettimeofday(&tp, NULL);
		double ms = double(tp.tv_usec) / 1000.;
		return tp.tv_sec*1000 + ms;
	}
}; // timer

}

#endif /* TIMER_H_ */
