#pragma once

#include <map>
#include <string>
using namespace std;

#include "lexicalCast.h"

namespace tools {

class commandLineParser {

public:

	// Create this class
	commandLineParser(int argc, char **argv) {
		// Parse command line arguments and create string map.
		for (int currentIndex = 1; currentIndex < argc; ++currentIndex) {
			if (argv[currentIndex][0] == '-') {
				string key(&argv[currentIndex][1]);

				// Create value (which might be the next argument actually)
				string value("");
				if ((currentIndex + 1) < argc) {
					value.assign(argv[currentIndex+1]);
				}

				// Add to map.
				arguments[key] = value;
			}
		}
	}

	// Get an argument's value (converted to something)
	template<typename T>
	inline T value(const string key, const T defaultValue = T()) {
		if (arguments.find(key) != arguments.end())
			return lexicalCast<T>(arguments[key]);
		else
			return defaultValue;
	}

	// Test wether an option is actually set.
	inline bool isSet(const string key) {
		return arguments.find(key) != arguments.end();
	}

private:

	// This map has all the command line arguments.
	map<string, string> arguments;
};

}
