
#include <string>
#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

#include "data_structures/typedefs.h"
#include "tools/commandLineParser.h"
#include "data_structures/graphs.h"
#include "data_structures/graphs/partition.h"
#include "algorithms/customizable.h"
#include "algorithms/bidijkstra.h"
#include "algorithms/dijkstra.h"
// Netter Fortschrittsbalken
#include "tools/progressBar.h"
#include "geometry/pathVisualizer.h"
#include "geometry/coordinates.h"


int main(int argn, char **argv) {

	// Kommandozeilenparser instanziieren.
	tools::commandLineParser clp(argn, argv);

	// Wenn kein Graph angegeben wurde, mit einem Fehler abbrechen.
	if (!clp.isSet("g")) {
		cout << "Kein Graph angegeben. Bitte mit `" << argv[0] << " -g <graph>` aufrufen." << endl;
		return 0;
	}

	// Wenn keine Partition angegeben wurde, mit einem Fehler abbrechen.
	if (!clp.isSet("p")) {
		cout << "Keine Partition angegeben. Bitte mit `" << argv[0] << " -p <partition>` aufrufen." << endl;
		return 0;
	}

	// Wenn keine Partition angegeben wurde, mit einem Fehler abbrechen.
	if (!clp.isSet("c")) {
		cout << "Keine Koordinaten angegeben. Bitte mit `" << argv[0] << " -c <coordinate>` aufrufen." << endl;
		return 0;
	}

	const int randomSeed = clp.value<int>("r", 31101982); // Saat f�r den Zufallszahlengenerator
	srand(randomSeed);

	// Graph-Dateinamen von der Kommandozeile einlesen.
	const string graphFilename = clp.value<string>("g", "");
	const string partFilename = clp.value<string>("p", "");
	const string coordinateFilename = clp.value<string>("c", "");
	const int numPhantomLevel = clp.value<int>("l", 0);

	graphs::roadGraph g(graphFilename);
	graphs::partition p(partFilename);

	// Get start and target node
	const types::NodeID source_node = clp.value<unsigned>("s", rand() % g.get_num_nodes());
	const types::NodeID target_node = clp.value<unsigned>("t", rand() % g.get_num_nodes());

	algorithms::customizable<graphs::roadGraph> customizable(g, p, numPhantomLevel);
	// Aufruf von unserem Algorithmus
	customizable.run(source_node, target_node);

	types::nodeContainer path;
	path.clear();
	customizable.unpackConnection(path);
	geometry::coordinates c;
	c.load(coordinateFilename);
	geometry::pathVisualizer pv(&c);
	pv.visualize(path, "out.kml");

	cout << " done." << endl << endl;

	return 0;
}
