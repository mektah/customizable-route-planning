/*
 * overlay.h
 *
 *  Created on: Dec 16, 2011
 *      Author: ameier
 */

#ifndef CUSTOMIZABLE_H_
#define CUSTOMIZABLE_H_

#include <vector>
#include <list>
#include <string>
#include <iostream>
#include <omp.h>
#include <math.h>
using namespace std;

#include "../data_structures/typedefs.h"
#include "../data_structures/graphs.h"
#include "../data_structures/pqueues/kheap.h"
#include "../data_structures/graphs/partition.h"
#include "../data_structures/graphs/overlay.h"
#include "../data_structures/graphs/metrics/metric.h"
#include "../tools/timer.h"
#include "../tools/pinThreads.h"

namespace algorithms {

template<typename graphType>
class customizable {

public:
	static const size_t MAX_CELLSIZE = 1048576;
	static const size_t MAX_THREADS = 8;

// Temporäre Suchinformationen über die Knoten
	struct node_label {
		types::EdgeWeight distance[MAX_THREADS >= 2 ? MAX_THREADS : 2];
		types::NodeID parent[2];
		types::LevelID parentEdgeLevel[2];
		unsigned timestamp[MAX_THREADS >= 2 ? MAX_THREADS : 2];
		bool settled[MAX_THREADS >= 2 ? MAX_THREADS : 2];
		node_label() {
			parentEdgeLevel[FORWARDS] = types::INFTY;
			parentEdgeLevel[BACKWARDS] = types::INFTY;
			parent[FORWARDS] = types::NULLNODE;
			parent[BACKWARDS] = types::NULLNODE;
			distance[FORWARDS] = types::INFTY;
			distance[BACKWARDS] = types::INFTY;
			settled[FORWARDS] = false;
			settled[BACKWARDS] = false;
			timestamp[FORWARDS] = 0;
			timestamp[BACKWARDS] = 0;
			for (int i = 2; i < MAX_THREADS; i++) {
				distance[i] = types::INFTY;
				settled[i] = false;
				timestamp[i] = 0;
			}
		}
	};

	typedef pqueues::kheap<4, types::NodeID, types::EdgeWeight> queueType;
	typedef graphs::metric<graphType> metricType;
// Bem.: metricID 0 = Metrik des Originalgraphen
	typedef types::MetricID metricID;
	typedef types::direction Direction;
	typedef types::NodeID nodeID;
	typedef types::CellID cellID;
	typedef types::LevelID levelID;
	typedef types::EdgeWeight edgeWeight;

public:

// Konstruktor
	customizable(graphType *g, graphs::partition *p, size_t numPhantomLevel = 0) :
			currentMetric(0), nodeLabels(g->get_num_nodes()),
			queues(MAX_THREADS >= 2 ? MAX_THREADS : 2, NULL), overlays(1, NULL), metrics(1, NULL), connectionBase(types::NULLNODE) {
		this->g = g;
		this->p = p;

		assert (numPhantomLevel >= 0 && numPhantomLevel <= p->getNumLevel());
		this->numPhantomLevel = numPhantomLevel;
		//queues[FORWARDS] = new queueType(g->get_num_nodes());
		//queues[BACKWARDS] = new queueType(g->get_num_nodes());
		globalTime[FORWARDS] = 0;
		globalTime[BACKWARDS] = 0;
		//Queues for parallelized Overlay
		#pragma omp parallel
		{
		tools::pinThread(omp_get_thread_num());
		queues[omp_get_thread_num()] = new queueType(g->get_num_nodes());
		globalTime[omp_get_thread_num()] = 0;
		//for (int i = 2; i < MAX_THREADS; i++) {
			//queues[i] = new queueType(g->get_num_nodes());
			//
		//}
		}
		metrics.clear();
		overlays.clear();
		// Standardmetrik von g hinzufügen
		metrics.push_back(new metricType(g));
		overlays.push_back(NULL);
		assert(metrics.size() == 1);
		assert(overlays.size() == 1);
		setMetric();
	}

// Destruktor
	~customizable() {
		if (queues[FORWARDS] != NULL) {
			delete queues[FORWARDS];
			queues[FORWARDS] = NULL;
		}
		if (queues[BACKWARDS] != NULL) {
			delete queues[BACKWARDS];
			queues[BACKWARDS] = NULL;
		}
		for (int i = 2; i < MAX_THREADS; i++) {
			if (queues[i] != NULL) {
				delete queues[i];
				queues[i] = NULL;
			}
		}
		for (int m = 1; m < metrics.size(); m++) {
			deleteMetric(m);
		}
	}

// For testing purposes: Be able to change the Phantomlevels during runtime
void setNumPhantomLevel(unsigned n) {
	assert(n <= p->getNumLevel());
	numPhantomLevel = n;
}

// Increase globalTime and check for overflow
void increaseTime(unsigned i) {
	assert(i < 2 || i < MAX_THREADS);
	globalTime[i]++;
	if (globalTime[i] == 0) {
		for (int j = 0; j < nodeLabels.size(); j++) {
			nodeLabels[j].distance[i] = types::INFTY;
			nodeLabels[j].settled[i] = false;
			nodeLabels[j].timestamp[i] = 0;
			if (i == FORWARDS || i == BACKWARDS) {
				nodeLabels[j].parent[i] = types::NULLNODE;
			}
		}
	}
}

// Run the algorithm between two nodes s and t (possibly NULLNODE)
// Remark: Query not parallelized yet!!
// Therefore always using timestamp[0] as timestamp and globalTime[0] as globalTime
	int run(const nodeID s, const nodeID t) {
		//increase timestamp, so node_labels do not have to be reset
		increaseTime(0); // = globalTime[0]++;

		tools::timer tim;
		nodeID currentBase[2];
		edgeWeight mu;
		graphs::overlay *o = overlays[currentMetric];
		assert(o != NULL);

		assert (FORWARDS != BACKWARDS);

		//shorthand to make things easier:
		currentBase[FORWARDS] = s;
		currentBase[BACKWARDS] = t;

		//reset
		queues[FORWARDS]->clear();
		queues[BACKWARDS]->clear();
		num_scanned_nodes = 0;
		num_relaxed_edges = 0;
		connectionBase = types::NULLNODE;

		assert(queues[FORWARDS]->empty());
		assert(queues[BACKWARDS]->empty());

		// Implementation
		queues[FORWARDS]->update(s, edgeWeight(0));
		queues[BACKWARDS]->update(t, edgeWeight(0));
		//vector < node_label > (g.get_num_nodes()).swap(nodeLabels); //not needed any more because of timestamps
		checktimestamp(s);
		checktimestamp(t);
		nodeLabels[s].distance[FORWARDS] = 0;
		nodeLabels[t].distance[BACKWARDS] = 0;
		mu = types::INFTY;

		//cout << "New run from " << s << " to " << t << endl;

		Direction dir = FORWARDS;
		while (!queues[dir]->empty()) {
			edgeWeight minDistance;
			nodeID minNode;
			nodeID connect;
			graphs::roadGraph::edgeType *e(NULL);
			num_scanned_nodes++;
			queues[dir]->extractMin(minNode, minDistance);
			assert(nodeLabels[minNode].settled[dir] == false);
			nodeLabels[minNode].settled[dir] = true;

			assert(nodeLabels[minNode].timestamp[0] == globalTime[0]);
			assert(minDistance >= 0);
			assert(minDistance != types::INFTY);
			assert(minDistance == nodeLabels[minNode].distance[dir]);
			assert(dir == FORWARDS || dir == BACKWARDS);

			//cout << "Extracted node " << minNode << " with Distance " << minDistance <<endl;

			//if this node already has a distance from the other direction, update tentative distance for s-t
			if (nodeLabels[minNode].distance[FORWARDS] != types::INFTY
					&& nodeLabels[minNode].distance[BACKWARDS] != types::INFTY) {

				edgeWeight oldmu = mu;
				mu = std::min(
						mu,
						nodeLabels[minNode].distance[FORWARDS]
								+ nodeLabels[minNode].distance[BACKWARDS]);
				if (mu != oldmu) {
					connectionBase = minNode;
					//cout << "Node " << minNode << " has Distances "
					//		<< nodeLabels[minNode].distance[FORWARDS] << " and ";
					//cout << nodeLabels[minNode].distance[BACKWARDS] << ". ";
					//cout << "Mu was " << oldmu << " and is ";
					//cout << mu << " now." << endl;
				}
			}

			levelID searchLevel = std::min(p->getLeastCommonLevel(s, minNode), p->getLeastCommonLevel(t,minNode)); //lcl := least Common Level
			//levelID lsbl = std::min(p.getMaxBorderLevel(minNode), p.getMaxBorderLevel(currentBase[dir]));
			//if (lsbl >= lcl) lcl = lsbl + 1; //if they are both border nodes, we can use the shortcuts
			if (searchLevel <= numPhantomLevel)
				searchLevel = 0;

			assert(searchLevel <= p->getNumLevel());
			assert(searchLevel >= 0);
			assert(!o->isLevelDeleted(searchLevel-1));
			//searchlevel-1 is to accomodate the different indices.
			//In the partition, level 0 is the base graph, in the overlay structure, level -1 is the base graph
			//selecting edge range in base graph and overlay graph
			o->selectNode(minNode, searchLevel - 1, dir); //TODO: check level specification
			e = g->get_first_edge(minNode);

			bool baseedge;
			//overlay edges and cut edges of the base graph are considered
			while (o->hasShortcutEdgesLeft() || e <= g->get_last_edge(minNode)) {
				nodeID connect;
				edgeWeight weight;
				if (o->hasShortcutEdgesLeft()) {
					assert(searchLevel > 0);
					o->getNextShortcutEdge(connect, weight); //TODO: need to give pointers?
					baseedge = false;
					//assert(p.getLeastCommonLevel(connect, minNode) +1 == p.getLeastCommonLevel(minNode, currentAim[dir]));
					//cout << "Found shortcut to ";
					if (weight == types::INFTY) {
						//an infinite weight means the target is not in the same connected component
						//assert (nodeLabels[connect].timestamp < globalTime || nodeLabels[connect].distance[dir] == types::INFTY);
						//TODO: make the above assertion work!
						if (nodeLabels[connect].timestamp[0] == globalTime[0] && nodeLabels[connect].distance[dir] != types::INFTY) {
							//cout << "Warning: shortcut to " << connect << "probably wrong!" << endl;
						}
					}
				} else {
					baseedge = true;
					//Now I want to connect cut edges in higher levels and work
					connect = e->node();
					//caution! only consider nodes of equal level!
					if (!e->has_direction(dir)
							|| p->getMaxBorderLevel(connect) + 1 < searchLevel) {
						assert(!e->has_direction(dir) || p->getLeastCommonLevel(minNode, connect) < searchLevel);
						e++;
						continue;
					}
					//cout << "Found cut edge to ";
					//assert(p.getLeastCommonLevel(minNode, connect) >= searchLevel || p.getLeastCommonLevel(minNode,connect) < numPhantomLevel);
					// get Edge Weights from metric
					assert(metrics[currentMetric] != NULL);
					weight = (metrics[currentMetric])->getWeight(g->get_edge_id(e));
					e++;
				}
				//cout << connect << " with weight " << weight <<endl;
				assert(weight >= 0);
				if (weight == types::INFTY) continue;
				num_relaxed_edges++;
				node_label *targetLabel = &(nodeLabels[connect]);
				assert (targetLabel->distance[dir] == nodeLabels[connect].distance[dir]);
				checktimestamp(connect);

				//cout << "Previous distance:" << targetLabel->distance[dir]<<endl;
				if (targetLabel->distance[dir] == types::INFTY)
					assert(!queues[dir]->contains(connect));
				if (minDistance + weight < targetLabel->distance[dir] && !sumOverflows(minDistance, weight)) {
					assert(targetLabel->settled[dir] == false);
					targetLabel->parent[dir] = minNode;
					targetLabel->distance[dir] = minDistance + weight;
					targetLabel->parentEdgeLevel[dir] = baseedge ? -1 : searchLevel - 1;
					assert(targetLabel->parentEdgeLevel[dir] <= p->getMaxBorderLevel(connect));
					//cout << "EdgeLevel" << targetLabel->parentEdgeLevel[dir] << " assigned to " << ((dir == FORWARDS) ? "forwards" : "backwards") << " edge between" << minNode << " and " << connect << endl;
					if (minDistance + weight <= mu || queues[dir]->contains(connect)) {
						queues[dir]->update(connect, minDistance + weight);  //TODO remove this discrepancy
						//cout << "Added " << connect << " to queue" << endl;
					} else {
						//the key of the priority queue shall not be different than the distance in the nodeLabel.
						//This wouldn't be a problem if we change the break condition to finish also when one queue is empty
					}
				}
				//cout << "New distance of" << connect << ": "<< targetLabel->distance[dir]<<endl;

				assert (nodeLabels[connect].distance[dir] <= minDistance + weight);}

			if (!queues[FORWARDS]->empty() && !queues[BACKWARDS]->empty()) {
				//Break here if smallest route is found
				if (queues[FORWARDS]->minKey() + queues[BACKWARDS]->minKey()
						> mu) {
					lastDistance = mu;
					elapsed_milliseconds = tim.elapsed();
					//cout << "Found connection!" << endl;
					return lastDistance;
				}

				if (queues[FORWARDS]->minKey() <= queues[BACKWARDS]->minKey())
					dir = FORWARDS;
				else
					dir = BACKWARDS;
			} else {
				if (queues[FORWARDS]->empty() && queues[FORWARDS]->empty()) {
					lastDistance = mu;
					elapsed_milliseconds = tim.elapsed();
					cout << "Queue is empty!" << endl;
					lastDistance = types::INFTY;
					return lastDistance;
				} else {
					if (queues[FORWARDS]->empty())
						dir = BACKWARDS;
					else {
						assert(queues[BACKWARDS]->empty());
						dir = FORWARDS;
					}
				}
			}
		}
		assert(false);
		elapsed_milliseconds = tim.elapsed();
	}

//	Statistikfunktionen
	inline unsigned get_num_scanned_nodes() {
		return num_scanned_nodes;
	}
	inline unsigned get_num_relaxed_edges() {
		return num_relaxed_edges;
	}
	inline unsigned get_elapsed_milliseconds() {
		return elapsed_milliseconds;
	}
	//Get the distance of a node
	inline types::EdgeWeight get_distance() {
		return lastDistance;
	}

	void unpackConnection(list<types::NodeID> &connection, list<types::LevelID> &levels, int recursionDepth = 0) {
			list<types::NodeID>::iterator it;
			list<types::LevelID>::iterator lit;
			//first, determine the number of nodes in the connection
			assert(connectionBase != types::NULLNODE);
			connection.clear();
			levels.clear();
			types::EdgeWeight previousDistance = get_distance();
			unsigned unpackTime = globalTime[0];

			//This is actually unnecessary
			unsigned connectionLength = 1;
			nodeID searchNode = connectionBase;
			while (nodeLabels[searchNode].parent[FORWARDS] != types::NULLNODE) {
				connectionLength++;
				searchNode = nodeLabels[searchNode].parent[FORWARDS];
				assert(nodeLabels[searchNode].timestamp[0] == unpackTime);
			}
			unsigned baseIndex = connectionLength -1;
			searchNode = connectionBase;
			while (nodeLabels[searchNode].parent[BACKWARDS] != types::NULLNODE) {
				connectionLength++;
				searchNode = nodeLabels[searchNode].parent[BACKWARDS];
				assert(nodeLabels[searchNode].timestamp[0] == unpackTime);
			}
			//resize vector accordingly
			//levels.resize(connectionLength);

			//now, write connection in return list
			searchNode = connectionBase;
			int i = baseIndex;
			levels.push_back(nodeLabels[searchNode].parentEdgeLevel[FORWARDS]);
			connection.push_back(connectionBase);

			while (nodeLabels[searchNode].parent[FORWARDS] != types::NULLNODE) {
				searchNode = nodeLabels[searchNode].parent[FORWARDS];
				levels.push_front(nodeLabels[searchNode].parentEdgeLevel[FORWARDS]);
				connection.push_front(searchNode);
			}
			searchNode = connectionBase;

			while (nodeLabels[searchNode].parent[BACKWARDS] != types::NULLNODE) {
				//nodeID oldSearchNode = searchNode;
				levels.push_back(nodeLabels[searchNode].parentEdgeLevel[BACKWARDS]);
				searchNode = nodeLabels[searchNode].parent[BACKWARDS];
				//nodeLabels[searchNode].parent[FORWARDS] = oldSearchNode;
				//nodeLabels[searchNode].parentEdgeLevel[FORWARDS] = searchLevel;
				//levels.push_back(searchLevel);
				connection.push_back(searchNode);
			}

			assert (connection.size() == connectionLength);
			assert (levels.size() == connection.size());

			//Now, remove the points one by one and search for the routes in the base graph

			bool baseConnection = true;
			do {
				baseConnection = true;
				nodeID previousNode = connection.front();
				lit = levels.begin();
				for (it = connection.begin(); it != connection.end(); it++) {
					nodeID currentNode = *it;
					if (connection.front() == currentNode) {
						lit++;
						continue;
					}
					assert(lit != levels.end());
					levelID searchLevel = *lit;
					assert (searchLevel <= min(p->getMaxBorderLevel(currentNode), p->getMaxBorderLevel(previousNode)));
					if (searchLevel >= 0) { //edge was a shortcut, we have to find the real one
						//cout << "Node " << currentNode << " reached through level " << searchLevel << " from " << previousNode << endl;
						//assert(searchLevel >= 0);
						baseConnection = false;
						//cout << "Edge from " << previousNode << " to " << currentNode << " was a shortcut. Initiating Dijkstra level " << recursionDepth+1 << endl;
						edgeWeight overlayDistance = overlays[currentMetric]->getDistance(previousNode, currentNode);
						edgeWeight custDistance = run(previousNode, currentNode);
						if (overlayDistance != custDistance) {
							cout << "overlayDistance: " << overlayDistance << ", Instanz von cust: " << custDistance << endl;
						}
						assert (overlayDistance >= custDistance);
						list<types::NodeID> relaxedShortcut;
						list<types::LevelID> shortCutLevels;
						unpackConnection(relaxedShortcut, shortCutLevels, recursionDepth + 1);
						assert(relaxedShortcut.front() == previousNode);
						assert(relaxedShortcut.back() == currentNode);
						assert(relaxedShortcut.size() == shortCutLevels.size());
						//cout << "Connection length increased from " << connection.size();
						connection.splice(it, relaxedShortcut);
						lit = levels.erase(lit);
						levels.splice(lit, shortCutLevels, ++shortCutLevels.begin(), shortCutLevels.end());
						int prevSize = connection.size();
						connection.unique();
						assert(prevSize == connection.size() +2);
						assert (levels.size() == connection.size());
						//cout << " to " << connection.size() << endl;
						break;
					}
					previousNode = currentNode;
					lit++;
				}
			} while (baseConnection == false);

			//now, check whether everything is correct
			types::EdgeWeight cumulatedDistance = 0;
			nodeID previousNode = connection.front();

			for (it = connection.begin(); it != connection.end(); it++) {
				nodeID currentNode = *it;
				if (connection.front() == currentNode) continue;
				assert(g->contains_arc(previousNode, currentNode) || g->contains_arc(currentNode, previousNode, BACKWARDS));
				types::EdgeWeight w;
				if (g->contains_arc(previousNode, currentNode)) {
					w = metrics[currentMetric]->getWeight(g->get_edge_id(g->get_arc(previousNode, currentNode)));
					if (g->contains_arc(currentNode, previousNode, BACKWARDS)) assert (w == metrics[currentMetric]->getWeight(g->get_edge_id(g->get_arc(currentNode, previousNode, BACKWARDS))));
				} else {
					w = metrics[currentMetric]->getWeight(g->get_edge_id(g->get_arc(currentNode, previousNode, BACKWARDS)));
				}

				cumulatedDistance += w;
				//cout << "Following edge with weight " << w << ", distance is now " << cumulatedDistance << endl;
				previousNode = currentNode;
			}
			if (cumulatedDistance != previousDistance) {
				cout << "Distance should be " << previousDistance << ", but is " << cumulatedDistance << endl;
			}
			assert(cumulatedDistance == previousDistance);
		}

// *** Dynamische Funktionen ***

// Overlay wird erst in diesem Schritt gebastelt!
	void setMetric(metricID m = 0) {
		if (overlays[m] == NULL) {
			buildOverlay(m);
		}
		currentMetric = m;
	}

// Benutzer muss sich Rückgabe merken, um hinzugefügte Metrik
// identifizieren zu können.
	metricID addMetric(metricType *M) {
		assert(M != NULL);
		unsigned s = metrics.size();
		metrics.push_back(M);
		assert(metrics.size() == s + 1);
		assert(metrics[metrics.size()-1] == M);
		// Platzhalter für Overlay hinzufügen
		overlays.push_back(NULL);
		//assert(overlays[0] != NULL);
		return (metrics.size() - 1);
	}

	void deleteMetric(metricID m) {
		if (m == 0) {
			cerr << "ERROR: Cannot delete metric of the original graph!"
					<< endl;
		} else {
			if (metrics[m] != NULL) {
				delete metrics[m];
				metrics[m] = NULL;
			}
			if (overlays[m] != NULL) {
				delete overlays[m];
				queues[m] = NULL;
			}
		}
	}

private:
	//TODO: Ggf. an Parallelisierte Query anpassen!
	void checktimestamp(nodeID v, int dir = 0) {
		if (nodeLabels[v].timestamp[dir] != globalTime[0]) {
		assert(nodeLabels[v].timestamp[dir] < globalTime[0]);
		nodeLabels[v].parentEdgeLevel[FORWARDS] = types::INFTY;
		nodeLabels[v].parentEdgeLevel[BACKWARDS] = types::INFTY;
		nodeLabels[v].distance[FORWARDS] = types::INFTY;
		nodeLabels[v].distance[BACKWARDS] = types::INFTY;
		nodeLabels[v].settled[FORWARDS] = false;
		nodeLabels[v].settled[BACKWARDS] = false;
		nodeLabels[v].parent[FORWARDS] = types::NULLNODE;
		nodeLabels[v].parent[BACKWARDS] = types::NULLNODE;
		nodeLabels[v].timestamp[dir] = globalTime[0];
		}
	}

// Overlay zu gegebener metricID basteln und speichern
	void buildOverlay(metricID m = 0) {
		overlays[m] = new graphs::overlay(p);
		graphs::overlay *o = overlays[m];
		// Verwende this->queues[0] für Dijkstras
		// Bem.: In Queue stehen nodeIDs des Graphen (nicht Matrix-Indizes!)
		for (int i = 0; i < MAX_THREADS; i++) {
			queues[i]->reset();
		}
		double timeSum = 0;
		for (levelID l = 0; l < p->getNumLevel(); l++) {
			cout << "Suche in Level " << l << "..." << flush;
			tools::timer tim;

			omp_set_num_threads(MAX_THREADS);

			unsigned allotment = (p->getNumCells(l) / MAX_THREADS) + 1;
			size_t chunkSize = allotment / 256;
			if (chunkSize == 0) chunkSize = 1;
			#pragma omp parallel //private(i, ID, currentCellPointer, lowerLevelCell, currentMatrixToNodeID, lowerLevelMatrixToNodeID, indexCount, currentIndex, q, u, v, d, w, e)
			{
			tools::pinThread(omp_get_thread_num());

			int i, ID;
			graphs::overlay::cell *currentCellPointer;
			graphs::overlay::cell *lowerLevelCell;
			vector<types::NodeID> *currentMatrixToNodeID;
			vector<types::NodeID> *lowerLevelMatrixToNodeID;

			unsigned indexCount, currentIndex;
			queueType *q;
			nodeID u, v;
			edgeWeight d, w;
			graphs::roadGraph::edgeType *e;
			int myTime = globalTime[ID];

			#pragma omp for schedule (dynamic, chunkSize)
				for (cellID c = 0; c < p->getNumCells(l); c++) {
				ID = omp_get_thread_num();
				//#pragma omp critical
				//cout << "Hello, I am Thread Nr. " << ID << endl;
				//#pragma omp end critical
				q = queues[ID];
				currentCellPointer = o->getCell(l, c);
				currentMatrixToNodeID = p->matrixIndexVector(l, c);
				assert(currentMatrixToNodeID->size() == currentCellPointer->size());
				//assert(currentMatrixToNodeID.size() == currentCellPointer->size());
				for (i = 0; i < currentCellPointer->size(); i++) {
					//increaseTime(ID); // = globalTime[ID]++;
					myTime++;
					assert(q->empty());
					q->update((*currentMatrixToNodeID)[i], 0);
					nodeLabels[(*currentMatrixToNodeID)[i]].distance[ID] = 0;
					nodeLabels[(*currentMatrixToNodeID)[i]].timestamp[ID] = myTime;
					nodeLabels[(*currentMatrixToNodeID)[i]].settled[ID] = false;

					//q->update(currentMatrixToNodeID[i], 0);
					//nodeLabels[currentMatrixToNodeID[i]].distance[ID] = 0;
					//nodeLabels[currentMatrixToNodeID[i]].timestamp[ID] = myTime;
					//nodeLabels[currentMatrixToNodeID[i]].settled[ID] = false;
					indexCount = 0;
					while (!q->empty()) {
						u = types::NULLNODE;
						d = types::INFTY;
						q->extractMin(u, d);
						assert(nodeLabels[u].timestamp[ID] == myTime);
						assert(!nodeLabels[u].settled[ID]);
						nodeLabels[u].settled[ID] = true;
						assert(d != types::INFTY);
						assert (d == nodeLabels[u].distance[ID]);
						//cout << "Extracted node " << u << " with weight " << d << endl;
						// Matrixeinträge für (i,u) setzen
						if (p->getMaxBorderLevel(u) >= l) {
							assert(currentCellPointer->getDistance(i, p->getMatrixIndex(u, l))
											== types::INFTY);
							currentCellPointer->setDistance(i, p->getMatrixIndex(u, l), d);
							indexCount++;
						}
						v = types::NULLNODE;
						w = types::INFTY;
						e = g->get_first_edge(u);
						unsigned j = 0;
						if (l > 0) {
							lowerLevelCell = o->getCell(l - 1, p->getCellID(u, l - 1));
							lowerLevelMatrixToNodeID = p->matrixIndexVector(l-1, p->getCellID(u, l - 1));
						}
						while (e <= g->get_last_edge(u) || (l > 0 && j < lowerLevelCell->size())) {
							if (e <= g->get_last_edge(u)) { // Verarbeite alle relevanten Kanten des Originalgraphen
								v = e->node();
								if (!(e->forward()) || p->getCellID(v, l) != c) {
									e++;
									continue;
								}
								w = metrics[m]->getWeight(g->get_edge_id(e));
								if (l > 0) {
									if (p->getMaxBorderLevel(v) < l - 1) //not the one we want
											{
										e++;
										continue;
									}
									if (p->getCellID(v, l - 1) == p->getCellID(u, l - 1)) {//covered by shortcuts
										//now make sure the shortcuts are not worse than the direct connection
										edgeWeight sc = lowerLevelCell->getDistance(p->getMatrixIndex(u, l - 1), p->getMatrixIndex(v, l-1));
										assert (sc <= w);
										e++;
										continue;
									}
								}
							} else { // Verarbeite Shortcuts
								assert (e > g->get_last_edge(u));
								assert (l > 0);
								assert(p->getMaxBorderLevel(u) >= l - 1);
								currentIndex = p->getMatrixIndex(u, l - 1);
								// Erst die aktuelle Zelle in Level l-1 abarbeiten...
								if (j == currentIndex) {
									j++;
									continue;
								}
								v = (*lowerLevelMatrixToNodeID)[j];
								w = lowerLevelCell->getDistance(currentIndex, j);
							}

							//Jetzt der levelunabhaengige Code
							if (nodeLabels[v].timestamp[ID] != myTime) {
								//cout << "Updating Timestamp of node " << v << " from " << nodeLabels[v].timestamp << " to " << globalTime << endl;
								nodeLabels[v].timestamp[ID] = myTime;
								nodeLabels[v].distance[ID] = types::INFTY;
								nodeLabels[v].settled[ID] = false;
							}
							assert(nodeLabels[v].timestamp[ID] == myTime);
							assert (w >= 0);
							if ((w < types::INFTY)
									&& (nodeLabels[v].distance[ID] > d + w) && !(sumOverflows(d,w))) {
								assert(!nodeLabels[v].settled[ID]);
								nodeLabels[v].distance[ID] = d + w;
								q->update(v, d + w);
							}

							if (e <= g->get_last_edge(u)) ++e;
							else j++;
							//END WHILE
						}
						//END WHILE
					}
					assert(ID == omp_get_thread_num());
					assert (indexCount <= currentCellPointer->size());
					if (indexCount < currentCellPointer->size()) {
						//cout << "indexCount too small: " << indexCount << " for cell of size " << currentCellPointer->size() << endl;
					}
					//END FOR MATRIXINDEX
				}

				//END FOR CELL
			}
			globalTime[ID] = myTime;
			//END PARALLEL
			}
			// Phantomlevel löschen
			if ((l > 0) && (l <= numPhantomLevel)) {
				// Nur zu Testzwecken auskommentiert!!!
				//o->deleteLevel(l - 1);
			}
			double elapsed = tim.elapsed();
			timeSum += elapsed;
			cout << elapsed << " ms" << endl;
			//END FOR LEVEL
		}
		cout << "Total running time: " << timeSum << endl;
		//END FUNCTION
	}

// *** Datenstrukturen ***

// The graph we are operating on
	graphType *g;
	graphs::partition *p;
	vector<metricType*> metrics;
	vector<graphs::overlay*> overlays;
	metricID currentMetric;
// Die ersten numPhantomLevel Level sind Phantomlevel
	size_t numPhantomLevel;

// Several statistical data
	unsigned num_scanned_nodes, num_relaxed_edges;
	double elapsed_milliseconds;
	static const Direction FORWARDS = types::FORWARD_DIR;
	static const Direction BACKWARDS = types::BACKWARD_DIR;

// The priority queues
	vector<queueType*> queues;
// Node labels
	vector<node_label> nodeLabels;

// The point where backwards and forwards met at the last run
	nodeID connectionBase;
	types::EdgeWeight lastDistance;

// Aktueller Zeitstempel (für mehrere Dijkstras)
	unsigned globalTime[MAX_THREADS >= 2 ? MAX_THREADS : 2];

// Checken, ob die Summe zweier Kantengewichte > types::INFTY ist (Überlauf)
	bool sumOverflows(edgeWeight a, edgeWeight b) {
		bool overflow = a+b < a || a+b < b;
		if (overflow) {
			cout << "Warning: Pathlength overflow!";
		}
		return overflow;
	}
};
}
#endif /* CUSTOMIZABLE_H_ */
