// Dijkstra's Algorithmus
#ifndef DIJKSTRA_H_
#define DIJKSTRA_H_

#include <vector>
#include <string>
#include <iostream>
using namespace std;


#include "../data_structures/typedefs.h"
#include "../data_structures/graphs.h"
#include "../data_structures/pqueues/kheap.h"

#include "../tools/timer.h"

namespace algorithms {


template<typename graphType>
class dijkstra {

public:

	// Node labels
	struct node_label {
		types::EdgeWeight distance;
		unsigned timeStamp;
		node_label() : distance(types::INFTY), timeStamp(0) {}
	};

	typedef pqueues::kheap<4, types::NodeID, types::EdgeWeight> queueType;


public:

	// Construct this class
	dijkstra(graphType &g) :
		graph(g),
		queue(g.get_num_nodes()),
		node_labels(g.get_num_nodes())
	{
	}


	// Run the algorithm between two nodes s and t (possibly NULLNODE)
	void run(const types::NodeID s, const types::NodeID t) {
		tools::timer tim;

		// Reset data structures.
		vector<node_label>(graph.get_num_nodes()).swap(node_labels);
		num_scanned_nodes = 0;
		num_relaxed_edges = 0;
		queue.clear();

		// Implementation
		queue.update(s, types::EdgeWeight (0));
		node_labels[s].distance = types::EdgeWeight (0);
		
		while(!queue.empty()) {
			num_scanned_nodes++;
			types::EdgeWeight minDistance;
			types::NodeID minNode;
			queue.extractMin(minNode, minDistance);
			assert(minDistance < types::INFTY);
			assert(node_labels[minNode].distance == minDistance);

			if (minNode == t) break; //target reached!
			graphs::roadGraph::edgeType *e ( NULL );
			FORALL_INCIDENT_EDGES(graph, minNode, e) {
				if (!e->forward()) continue;
				if (e->weight() == types::INFTY) continue;
				num_relaxed_edges++;
				types::NodeID target = e->node();
				if (node_labels[minNode].distance + e->weight() < node_labels[target].distance) {
					node_labels[target].distance = node_labels[minNode].distance + e->weight();
					queue.update(target, node_labels[target].distance);
				}
			}
		}
		elapsed_milliseconds = tim.elapsed();
	}




	// Get the distance of a node
	inline types::EdgeWeight get_distance(const types::NodeID u) { return node_labels[u].distance; }

	// Return several stats
	inline unsigned get_num_scanned_nodes() { return num_scanned_nodes; }
	inline unsigned get_num_relaxed_edges() { return num_relaxed_edges; }
	inline unsigned get_elapsed_milliseconds() { return elapsed_milliseconds; }


private:
	// The graph we are operating on
	graphType graph;

	// The priority queue
	queueType queue;

	// Node labels
	vector<node_label> node_labels;

	// Several statistical data
	unsigned num_scanned_nodes, num_relaxed_edges;
	double elapsed_milliseconds;
};

}



#endif /* DIJKSTRA_H_ */
