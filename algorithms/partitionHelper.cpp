/*
 * partitionHelper.cpp
 *
 *  Created on: Dec 16, 2011
 *      Author: ameier
 */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <queue>
#include <algorithm>
#include "../tools/commandLineParser.h"
#include "../data_structures/graphs.h"
#include "../data_structures/graphs/partition.h"
#include "../geometry/coordinates.h"

const int numLevel = 4;

using namespace std;

// Concatenate a string with an integer.
void concatenation(string &s, int a) {
	stringstream ss;
	string tmp;
	ss << a;
	ss >> tmp;
	s.append(tmp);
}

// The data we need for each node.
struct node_label {
	types::LevelID maxBorderLevel;
	vector<types::CellID> cell;
	bool scanned, inserted;
	node_label() : maxBorderLevel(-1), scanned(false), inserted(false) {
		vector<types::CellID>(numLevel).swap(cell);
		for (int i = 0; i < numLevel; i++) {
			cell[i] = -1;
		}
	}
};

// Function to change the direction.
types::direction oppDir(types::direction dir) {
	if (dir == types::FORWARD_DIR) {
		return types::BACKWARD_DIR;
	} else {
		return types::FORWARD_DIR;
	}
}

// Compress the graph via edge-compression.
void compressGraph(graphs::roadGraph &g, bool verbose = true) {
	if (verbose) cout << "Compressing graph..." << flush;
	unsigned deleteCounter = 0;
	types::NodeID u = types::NULLNODE;
	graphs::roadGraph::edgeType *e(NULL);
	FORALL_EDGES(g, u, e) {
		types::direction dir = e->has_direction(types::FORWARD_DIR) ? types::FORWARD_DIR : types::BACKWARD_DIR;
		// Don't compress already compressed edges.
		if (e->has_direction(oppDir(dir))) continue;
		types::NodeID v = e->node();
		graphs::roadGraph::edgeType *f(NULL);
		for (f = g.get_first_edge(v); f <= g.get_last_edge(v); ++f) {
			// If there is a valid, directed edge (v, u) with the same weight as e: delete it!
			// and set (u, v) as bidirected edge.
			if ((f->valid()) && (f->has_direction(dir)) && (!(f->has_direction(oppDir(dir))))
					&& (f->node() == u) && (f->weight() == e->weight())) {
				if (dir == types::FORWARD_DIR) {
					e->set_backward();
					g.delete_edge(v,f);
					f--;
				} else {
					f->set_forward();
					g.delete_edge(u, e);
					e--;
				}
				deleteCounter++;
			}
		}
	}
	g.compress();
	if (verbose) cout << "done! (Deleted " << deleteCounter << " edges)" << endl;
}

// The comparison criterion for sorting the nodes of the graph.
bool operator< (node_label a, node_label b) {
	return a.cell[0] < b.cell[0];
}
bool operator<= (node_label a, node_label b) {
	return !(b < a);
}

void recursiveSort(vector<types::NodeID> &toSort, vector<node_label> &nodes, int l, int r, unsigned recursionDepth);

int main(int argn, char **argv){
	tools::commandLineParser clp(argn, argv);
	// If no graph is set --> Error!
	if (!clp.isSet("g")) {
		cout << "No graph declared. Please use `" << argv[0] << " -g <graph>`." << endl;
		return 0;
	}
	// Check the number of partition files to be equal to numLevels.
	// WARNING:	- Level-files are with starting-index 1.
	//			- Cells are with starting index 0.
	for (int i = 1; i <= numLevel; i++) {
		string levelStr("l");
		concatenation(levelStr, i);
		if (!clp.isSet(levelStr.c_str())) {
			cerr << "No Cut-File found for Level " << i << endl;
			return 0;
		}
	}
	// Load the graph and compress it.
	const string graphFilename = clp.value<string>("g", "");
	graphs::roadGraph graph(graphFilename);
	compressGraph(graph);
	vector<node_label> nodes;
	vector<node_label>(graph.get_num_nodes()).swap(nodes);

	cout << "Reading partition files..." << flush;
	// Read the number of cells for each level.
	vector<size_t> numCells;
	vector<size_t>(numLevel).swap(numCells);
	for (int i = 0; i < numLevel; i++) {
		string levelStr("l");
		concatenation(levelStr, i+1);
		string fileString = clp.value<string>(levelStr.c_str(), "");
		ifstream is;
		is.open(fileString.c_str());
		string line;
		getline(is, line);
		numCells[i] = atoi((line.substr(2, line.length()-2)).c_str());
		is.close();
	}
	// Queue for the BFS.
	queue<types::NodeID> borderQueue;

	// Fill the node_labels and the border queue.
	for (int i = numLevel-1; i >= 0; i--) {
		string levelStr("l");
		concatenation(levelStr, i+1);
		string fileString = clp.value<string>(levelStr.c_str(), "");
		ifstream is;
		is.open(fileString.c_str());
		string line;
		getline(is, line);
		line.erase();
		// Process border nodes.
		while (is.is_open() && is.good()) {
			getline(is, line);
			if (line.length() > 0) {
				const size_t tmpIndex = line.rfind(' ', line.length());
				const unsigned currentNode =
						atoi((line.substr(2, tmpIndex-2)).c_str());
				const unsigned currentCell =
						atoi((line.substr(tmpIndex, line.length()-tmpIndex)).c_str());

				if (nodes[currentNode].maxBorderLevel == -1) {
					nodes[currentNode].maxBorderLevel = i;
				}
				nodes[currentNode].cell[i] = currentCell;
				if (!nodes[currentNode].inserted) {
					borderQueue.push(currentNode);
					nodes[currentNode].inserted = true;
				}
			}
		}
		is.close();
	}
	cout << "done!" << endl;

	cout << "Doing BFS..." << endl;
	// Top-Down BFS: Queue is sorted w.r.t. maxBorderLevel.
	while (!borderQueue.empty()) {
		types::NodeID currentNode = borderQueue.front();
		borderQueue.pop();
		// Remark: Border nodes are already marked scanned.
		queue<types::NodeID> BFSQueue;
		if (!nodes[currentNode].scanned) BFSQueue.push(currentNode);
		types::LevelID currLevel = nodes[currentNode].maxBorderLevel;
		while(!BFSQueue.empty()) {
			types::NodeID u = BFSQueue.front();
			BFSQueue.pop();
			nodes[u].scanned = true;
			types::LevelID l = nodes[u].maxBorderLevel;
			graphs::roadGraph::edgeType *e(NULL);
			FORALL_INCIDENT_EDGES(graph,u,e) {
				types::NodeID v = e->node();
				types::LevelID vBorderLevel = nodes[v].maxBorderLevel;
				if ((!nodes[v].scanned) &&
						((vBorderLevel < currLevel) || (nodes[v].cell[currLevel] == nodes[currentNode].cell[currLevel]))) {
					// Inherit cell membership of v from level l to numLevel
					// for all u in same cell as v in level l.
					for (types::LevelID i = nodes[v].maxBorderLevel+1; i < numLevel; i++) {
						nodes[v].cell[i] = nodes[u].cell[i];
					}
					BFSQueue.push(v);
					// ... Tuning.
					nodes[v].scanned = true;
				}
			}
		}
	}
	cout << "done!" << endl;

	cout << "Sorting nodes..." << flush;
		vector<types::NodeID> tmpNodes;
	vector<types::NodeID>(graph.get_num_nodes()).swap(tmpNodes);
	for (types::NodeID v = 0; v < graph.get_num_nodes(); v++) {
		tmpNodes[v] = v;
	}
	recursiveSort(tmpNodes, nodes, 0, tmpNodes.size() - 1, 0);
	// SortedNodes[nodeID] = sorted index.
	vector<types::NodeID> sortedNodes;
	vector<types::NodeID>(graph.get_num_nodes()).swap(sortedNodes);
	for (types::NodeID v = 0; v < graph.get_num_nodes(); v++) {
		sortedNodes[v] = graph.get_num_nodes() - v - 1;
	}
	for (types::NodeID v = 0; v < graph.get_num_nodes(); v++) {
		sortedNodes[tmpNodes[v]] = v;
	}
	cout << "done!" << endl;
	cout << "Verifying sortation..." << flush;
	for (int i = 0; i < graph.get_num_nodes()-1; i++) {
		if (nodes[tmpNodes[i+1]] < nodes[tmpNodes[i]])
			cerr << "Warning: Inconsistend sorting at position " << i << ": "
			     << nodes[tmpNodes[i+1]].cell[0] << " should be greater equal "
			     << nodes[tmpNodes[i]].cell[0] << endl;
		if (sortedNodes[i] == types::NULLNODE) {
			cerr << "Warning: Missing node data" << endl;
		}
	}
	cout << "done!" << endl;

	cout << "Building sorted graph..." << endl;
	graphs::roadGraph sortedGraph;
	sortedGraph.add_nodes(graph.get_num_nodes());
	graphs::roadGraph::edgeType *e(NULL);
	// Build sorted graph by adding sorted edges successively.
	FORALL_EDGES(graph,u,e) {
		graphs::roadGraph::edgeType *f;
		f = sortedGraph.add_edge(sortedNodes[u], sortedNodes[e->node()]);
		*f = *e;
		f->set_node(sortedNodes[e->node()]);
	}
	cout << "done!" << endl;
	cout << "Saving sorted graph..." << endl;
	sortedGraph.save("1_" + graphFilename);
	cout << "done!" << endl;
	// We don't need the sorted graph anymore.
	sortedGraph.clear();
	cout << "Building and saving partition data...";
	graphs::partition P(graph.get_num_nodes(), numLevel, numCells);
	FORALL_NODES(graph, u) {
		for(types::LevelID l = 0; l < numLevel; l++) {
			P.insert(sortedNodes[u], l, nodes[u].cell[l], (nodes[u].maxBorderLevel >= l));
		}
	}
	P.save("1_" + graphFilename);
	cout << "done!" << endl;
	if (!clp.isSet("c")) {
		return 0;
	}

	cout << "Building and saving coordinate data...";
	const string coordinatesFilename = clp.value<string>("c", "");
	geometry::coordinates c(0);
	c.load(coordinatesFilename);
	geometry::coordinates cSorted(0);
	cSorted.load(coordinatesFilename);
	FORALL_NODES(graph, u) {
		cSorted[u] = c[sortedNodes[u]];
	}
	cSorted.save("1_" + graphFilename + ".co");
	cout << "done!" << endl;
	return 0;
}

// Helper function for divide
void swap(vector<types::NodeID> &v, int i, int j) {
	//cout << "Swapping " << i << " and " << j << " with array-size " << v.size() << endl;
	assert((i < v.size()) && (j < v.size()) && (i >= 0) && (j >= 0));
	types::NodeID tmp = v[i];
	v[i] = v[j];
	v[j] = tmp;
}

// Helper function for recursiveSort
unsigned divide(vector<types::NodeID> &toSort, vector<node_label> &nodes, int l, int r) {
	int myBetterPivotIndex = (int)((l+r)/2);
	assert(myBetterPivotIndex >= l && myBetterPivotIndex <= r);
	swap(toSort, myBetterPivotIndex, r);
	node_label pivot = nodes[toSort[r]];
	int i = l;
	int j = r;
	do {
		while (nodes[toSort[i]] <= pivot && i < r) {
			i++;
		}
		while (pivot <= nodes[toSort[j]] && j > l) {
			j--;
		}
		if (i < j) {
			swap(toSort, i, j);
		}
	}
	while (i < j);
	if (pivot < nodes[toSort[i]]) {
		swap(toSort, i, r);
	}
	return i;
}

// Quicksort for sorting the nodes of the graph w.r.t. the operator definded above
void recursiveSort(vector<types::NodeID> &toSort, vector<node_label> &nodes, int l, int r, unsigned recursionDepth) {
	if (l >= r) return;
	//cout << "Arrived at recursion depth " << recursionDepth << endl;
	int p = divide(toSort, nodes, l, r);
	//cout << "Going left into " << l << " to " << p-1 << endl;
	recursiveSort(toSort, nodes, l, p-1, recursionDepth+1);
	//cout << "Going right into " << p << " to " << r << endl;
	recursiveSort(toSort, nodes, p+1, r, recursionDepth+1);
}
