// Dijkstra's Algorithmus
#ifndef BIDIJKSTRA_H_
#define BIDIJKSTRA_H_

#include <vector>
#include <string>
#include <iostream>
using namespace std;


#include "../data_structures/typedefs.h"
#include "../data_structures/graphs.h"
#include "../data_structures/pqueues/kheap.h"

#include "../tools/timer.h"

namespace algorithms {


template<typename graphType>
class bidijkstra {

public:

	// Node labels
	struct node_label {
		types::EdgeWeight forwardDistance;
		types::EdgeWeight backwardDistance;
		bool forwardSettled;
		bool backwardSettled;
		node_label() : forwardDistance(types::INFTY), backwardDistance(types::INFTY) {
			forwardSettled = false;
			backwardSettled = false;
		}
	};

	typedef pqueues::kheap<4, types::NodeID, types::EdgeWeight> queueType;


public:

	// Construct this class
	bidijkstra(graphType &g) :
		graph(g),
		forwardQueue(g.get_num_nodes()),
		backwardQueue(g.get_num_nodes()),
		node_labels(g.get_num_nodes())
	{
	}


	// Run the algorithm between two nodes s and t (possibly NULLNODE)
	void run(const types::NodeID s, const types::NodeID t) {
		tools::timer tim;

		types::EdgeWeight minDistance;
		types::NodeID minNode;
		graphs::roadGraph::edgeType *e ( NULL );
		types::EdgeWeight oppDis;

		// Reset data structures.
		vector<node_label>(graph.get_num_nodes()).swap(node_labels);
		num_scanned_nodes = 0;
		num_relaxed_edges = 0;
		forwardQueue.reset();
		backwardQueue.reset();
		queueType *queue;

		// Implementation
		forwardQueue.update(s, types::EdgeWeight (0));
		backwardQueue.update(t, types::EdgeWeight (0));
		node_labels[s].forwardDistance = types::EdgeWeight (0);
		node_labels[t].backwardDistance = types::EdgeWeight (0);
		mu = types::INFTY;
		bool forwards = false;
		queue = &forwardQueue;
		
		while(!(forwardQueue.empty() || backwardQueue.empty())) { //trusting user input! bad!
			//if (forwardQueue.minKey() + backwardQueue.minKey() >= mu) break;

			queue = forwards ? &forwardQueue : &backwardQueue;
			num_scanned_nodes++;
			queue->extractMin(minNode, minDistance);
			assert(minDistance < types::INFTY);
			types::EdgeWeight distance = forwards ? node_labels[minNode].forwardDistance : node_labels[minNode].backwardDistance;
			assert(distance < types::INFTY);

			if (forwards) {
				node_labels[minNode].forwardSettled = true;
				if (node_labels[minNode].backwardDistance != types::INFTY && node_labels[minNode].backwardDistance + minDistance < mu)
					mu = node_labels[minNode].backwardDistance + minDistance;
				if (node_labels[minNode].backwardSettled) break;
			} else {
				node_labels[minNode].backwardSettled = true;
				if (node_labels[minNode].forwardDistance != types::INFTY && node_labels[minNode].forwardDistance + minDistance < mu)
					mu = node_labels[minNode].forwardDistance + minDistance;
				if (node_labels[minNode].forwardSettled) break;
			}

			FORALL_INCIDENT_EDGES(graph, minNode, e) {
				if (forwards && !e->forward()) continue;
				if ((!forwards) && !(e->backward())) continue;
				if (e->weight() == types::INFTY) continue;
				num_relaxed_edges++;
				types::NodeID target = e->node();
				node_label &targetLabel = node_labels[target];
				types::EdgeWeight &targetDistance = forwards ? targetLabel.forwardDistance : targetLabel.backwardDistance;
				oppDis = forwards ? targetLabel.backwardDistance : targetLabel.forwardDistance;
				if (minDistance + e->weight() < targetDistance) {
					targetDistance = minDistance + e->weight();
					queue->update(target, targetDistance);
					//if (oppDis != types::INFTY && oppDis + targetDistance < mu) mu = oppDis + targetDistance;
				}
			}
			forwards = (forwardQueue.minKey() <= backwardQueue.minKey());

		}
		
		elapsed_milliseconds = tim.elapsed();
	}




	// Get the distance of a node
	inline types::EdgeWeight get_distance(const types::NodeID u) { return mu; }

	// Return several stats
	inline unsigned get_num_scanned_nodes() { return num_scanned_nodes; }
	inline unsigned get_num_relaxed_edges() { return num_relaxed_edges; }
	inline unsigned get_elapsed_milliseconds() { return elapsed_milliseconds; }


private:
	// The graph we are operating on
	graphType graph;

	// The priority queue
	queueType forwardQueue;
	queueType backwardQueue;

	// Node labels
	vector<node_label> node_labels;

	// Several statistical data
	unsigned num_scanned_nodes, num_relaxed_edges;
	double elapsed_milliseconds;
	types::EdgeWeight mu;
};

}



#endif /* BIDIJKSTRA_H_ */
