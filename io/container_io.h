/*
 * container_io.h
 *
 *  Created on: Nov 10, 2010
 *      Author: pajor
 */

#ifndef CONTAINER_IO_H_
#define CONTAINER_IO_H_

#include <fstream>
#include <string>

#include "string_io.h"

namespace io {


static void read_item(ifstream &f, string &item) {
	read_string_binary(item, f);
}

template<typename T>
static void read_item(ifstream &f, T &item) {
	f.read(reinterpret_cast<char*>(&item), sizeof(T));
}


static void write_item(ofstream &f, const string &item) {
	write_string_binary(item, f);
}

template<typename T>
static void write_item(ofstream &f, const T &item) {
	f.write(reinterpret_cast<const char*>(&item), sizeof(T));
}


/* Read a binary vector */
template<typename T>
static void read_vector(vector<T> & arr, const string &filename, const bool verbose = true) {
	if (verbose) cout << "Reading binary from " << filename << "... " << flush;
	// open binary file for reading
	ifstream in(filename.c_str(), ios::binary);
	if (!in) {
		if (verbose) cout << "Error opening " << filename << endl;
		return;
	}
	unsigned int size;
	in.read(reinterpret_cast<char*>(&size), sizeof(size));
	arr.resize(size);
	for (unsigned int i = 0; i < size; ++i) {
		T t;
		read_item(in, t);
		arr[i] = t;
	}
	in.close();

	if (verbose) cout << "done (" << size << " items)!" << endl;
}

/* Read a 2-dimensional binary vector */
template<typename T>
static void read_vector(vector<vector<T> > & arr, const string &filename, const bool verbose = true) {
	if (verbose) cout << "Reading binary from " << filename << "... " << flush;
	// open binary file for reading
	ifstream in(filename.c_str(), ios::binary);
	if (!in) {
		if (verbose) cout << "Error opening " << filename << endl;
		return;
	}
	unsigned int size0;
	in.read(reinterpret_cast<char*>(&size0), sizeof(size0));
	arr.resize(size0);
	for (unsigned int i = 0; i < size0; ++i) {
		unsigned int size1;
		in.read(reinterpret_cast<char*>(&size1), sizeof(size1));
		arr[i].resize(size1);
		for (unsigned int j = 0; j < size1; ++j) {
			T t;
			read_item(in, t);
			arr[i][j] = t;
		}
	}
	in.close();
	if (verbose) cout << "done!" << endl;
}

/* Read a 3-dimensional binary vector */
template<typename T>
static void read_vector(vector<vector<vector<T> > > & arr, const string &filename, const bool verbose = true) {
	if (verbose) cout << "Reading binary from " << filename << "... " << flush;
	// open binary file for reading
	ifstream in(filename.c_str(), ios::binary);
	if (!in) {
		if (verbose) cout << "Error opening " << filename << endl;
		return;
	}
	unsigned int size0;
	in.read(reinterpret_cast<char*>(&size0), sizeof(size0));
	arr.resize(size0);
	for (unsigned int i = 0; i < size0; ++i) {
		unsigned int size1;
		in.read(reinterpret_cast<char*>(&size1), sizeof(size1));
		arr[i].resize(size1);
		for (unsigned int j = 0; j < size1; ++j) {
			unsigned int size2;
			in.read(reinterpret_cast<char*>(&size2), sizeof(size2));
			arr[i][j].resize(size2);
			for (int k = 0; k < size2; k++) {
				T t;
				read_item(in, t);
				arr[i][j][k] = t;
			}
		}
	}
	in.close();
	if (verbose) cout << "done!" << endl;
}

/* Write a binary vector */
template<typename T>
static void write_vector(const vector<T> & arr, const string &filename, const bool verbose = true) {
	unsigned int size = arr.size();
	if (verbose) cout << "Writing binary to " << filename << "... " << flush;
	ofstream f(filename.c_str(), ios::binary);
	if (!f) {
		if (verbose) cout << "Error opening " << filename << endl;
		return;
	}
	f.write(reinterpret_cast<const char*>(&size), sizeof(size));
	for(unsigned int i = 0; i < size; i++)
		write_item(f, arr[i]);
	f.close();
	if (verbose) cout << "done (" << size << " items)!" << endl;
}

/* Write a 2-dimensional binary vector */
template<typename T>
static void write_vector(const vector<vector <T> > & arr, const string &filename, const bool verbose = true) {
	unsigned int size0 = arr.size();
	if (verbose) cout << "Writing binary to " << filename << "... " << flush;
	ofstream f(filename.c_str(), ios::binary);
	if (!f) {
		if (verbose) cout << "Error opening " << filename << endl;
		return;
	}
	f.write(reinterpret_cast<const char*>(&size0), sizeof(size0));
	for(unsigned int i = 0; i < size0; i++) {
		unsigned int size1 = arr[i].size();
		f.write(reinterpret_cast<const char*>(&size1), sizeof(size1));
		for(unsigned int j = 0; j < size1; j++) {
			write_item(f, arr[i][j]);
		}
	}
	f.close();
	if (verbose) cout << "done!" << endl;
}

/* Write a 3-dimensional binary vector */
template<typename T>
static void write_vector(const vector<vector <vector<T> > > & arr, const string &filename, const bool verbose = true) {
	unsigned int size0 = arr.size();
	if (verbose) cout << "Writing binary to " << filename << "... " << flush;
	ofstream f(filename.c_str(), ios::binary);
	if (!f) {
		if (verbose) cout << "Error opening " << filename << endl;
		return;
	}
	f.write(reinterpret_cast<const char*>(&size0), sizeof(size0));
	for(unsigned int i = 0; i < size0; i++) {
		unsigned int size1 = arr[i].size();
		f.write(reinterpret_cast<const char*>(&size1), sizeof(size1));
		for(unsigned int j = 0; j < size1; j++) {
			unsigned int size2 = arr[i][j].size();
			f.write(reinterpret_cast<const char*>(&size2), sizeof(size2));
			for(unsigned int k = 0; k < size2; k++) {
				write_item(f, arr[i][j][k]);
			}
		}
	}
	f.close();
	if (verbose) cout << "done!" << endl;
}

}

#endif /* CONTAINER_IO_H_ */
