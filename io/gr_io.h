/*
 * gr_io.h
 *
 *  Created on: Nov 11, 2010
 *      Author: pajor
 */

#ifndef GR_IO_H_
#define GR_IO_H_

#include <fstream>
#include <string>
using namespace std;

#include "string_io.h"

namespace io {

// This retrieves the graph class for a certain file.
string get_graph_class(const string filename, const bool verbose=false) {
	ifstream f(filename.c_str(), ios::binary);

	if (!f.is_open()) {
		if (verbose) cout << "Error opening file " << filename << " for retrieving graph class" << endl;
		return "FAILED";
	}

	// Read the class
	string gc("UNKNOWN");
	read_string_binary(gc, f);

	f.close();

	return gc;
}

}

#endif /* GR_IO_H_ */
