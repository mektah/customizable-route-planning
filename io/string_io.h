/*
 * string_io.h
 *
 *  Created on: Nov 4, 2010
 *      Author: pajor
 */

#ifndef STRING_IO_H_
#define STRING_IO_H_

#include <cassert>
#include <string>
#include <fstream>

using namespace std;


namespace io {

// Write a string to a binary file stream
void write_string_binary(const string &str, ofstream &os) {
	assert(os.is_open());
	unsigned int sz = str.size();
	os.write(reinterpret_cast<char*>(&sz), sizeof(unsigned int));
	os.write(str.data(), sz);
}

// Read a string from a binary file stream
void read_string_binary(string &str, ifstream &is) {
	assert(is.is_open());
	unsigned int sz(0);
	is.read(reinterpret_cast<char*>(&sz), sizeof(unsigned int));
//	is.ignore(sz);
	str.resize(sz);
	is.read(&str[0], sz);
}

}

#endif /* STRING_IO_H_ */
